 This program demonstrates the 5G N2 handover protocol. 
 
 There are C++ classes representing each entity and each of these runs a state
 machine that manages the protocol. Each entity runs in its own thread. 

 There is no attempt to reproduce the network layers, instead there is a message 
 passing protocol. Messages are passed up and down the 'stack' from the Mobile
 Entities (MEs) to the other entities in the system.

There can be several MEs and and several gNBs, but only one of each other entity
involved in the handovers.

 

