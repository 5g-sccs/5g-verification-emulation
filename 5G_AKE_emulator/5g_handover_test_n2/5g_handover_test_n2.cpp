/*******************************************************************************
 * File:        5g_handover_test_n2.cpp
 * Description: Test of the 5G handover protocol using N2
 *
 * Created:     Friday 5 May 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <thread>
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include "Openssl_utils.h"
#include "Model_param.h"
#include "MT_queue.h"
#include "Entity_thread.h"
#include "Msg_handler.h"
#include "Msg_router.h"
#include "Io_utils.h"
#include "Msg_utils.h"
#include "Byte_buffer.h"
#include "Key_derivation.h"
#include "milenage_f1to5.h"
#include "gNB_entity.h"
#include "Mobile_entity.h"
#include "Amf_entity.h"
#include "Arpf_entity.h"
#include "Ausf_entity.h"
#include "Ecies_utils.h"
#include "Entity_utils.h"

int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::cout << argv[0] << " an initial test for N2 handover\n";
        std::cout << "Usage: " << argv[0] << " <HO re-keyed (T/F)>\n";
        return EXIT_FAILURE;
    }

    // Just take the first character of argv[1] - don't bother checking the
    // length
    char ho_c =
      static_cast<char>(std::toupper(static_cast<unsigned char>(argv[1][0])));

    if (ho_c != 'T' && ho_c != 'F') {
        std::cout << "Please enter T or F\n";
        std::cout << "Usage: " << argv[0] << " <second HO re-keyed (T/F)>\n";
        return EXIT_FAILURE;
    }

    Nasc_kamf_change_flag kcf = (ho_c == 'T') ? changed : unchanged;

    init_openssl();

    try {
        // Do the UE side first
        EC_KEY *op_pub_key = get_op_public_key_from_file(op_key_basename);
        if (op_pub_key == nullptr) {
            std::cout << "Fetching the operator's public key failed\n";
            exit(1);
        }

        // Now do the Operator side
        EC_KEY *op_priv_key = get_op_private_key_from_file(op_key_basename);
        if (op_priv_key == nullptr) {
            std::cerr << "Fetching the operator's private key failed\n";
            exit(1);
        }

        Entity_queues msg_queues;
        Entity_thread_data router_thread;
        Entity_threads_map entity_threads_map;

        // Initialise the message router, although not an Entity, it needs an ID
        Mesg_router msg_router(router_base_ID);
        msg_router.assign_input_queue(msg_queues.get_queue());
        router_thread.id_ = router_base_ID;
        router_thread.qptr_ = msg_router.get_input_queue_ptr();

        // Set up ARPF entity
        Id_type id = arpf_base_ID;
        ARPF_entity arpf_e(id, opd, op_priv_key);
        arpf_e.add_user(usimd1);
        arpf_e.add_user(usimd2);
        Entity_msg_handler arpf_mh(id, &arpf_e);
        setup_entity(
          arpf_e, arpf_mh, msg_queues, msg_router, entity_threads_map);

        // Set up AUSF entity
        id = ausf_base_ID;
        AUSF_entity ausf_e(id);
        Entity_msg_handler ausf_mh(id, &ausf_e);
        setup_entity(
          ausf_e, ausf_mh, msg_queues, msg_router, entity_threads_map);

        // Set up AMF entity
        id = amf_base_ID;
        AMF_entity amf_e(id);
        Entity_msg_handler amf_mh(id, &amf_e);
        setup_entity(amf_e, amf_mh, msg_queues, msg_router, entity_threads_map);

        // Set up gNB entities
        id = gNB_base_ID;
        gNB_entity gNB_e(id);
        Entity_msg_handler gNB_mh(id, &gNB_e);
        setup_entity(gNB_e, gNB_mh, msg_queues, msg_router, entity_threads_map);

        id = gNB_base_ID + 1;
        gNB_entity gNB2_e(id);
        Entity_msg_handler gNB2_mh(id, &gNB2_e);
        setup_entity(
          gNB2_e, gNB2_mh, msg_queues, msg_router, entity_threads_map);

        id = gNB_base_ID + 2;
        gNB_entity gNB3_e(id);
        Entity_msg_handler gNB3_mh(id, &gNB3_e);
        setup_entity(
          gNB3_e, gNB3_mh, msg_queues, msg_router, entity_threads_map);

        // Set up mobile entity (UE)
        id = UE_base_ID;
        Usim us(usimd1, opd, op_pub_key);
        Mobile_entity me(id, us);
        Entity_msg_handler me_mh(id, &me);
        setup_entity(me, me_mh, msg_queues, msg_router, entity_threads_map);

        // Assign ID's
        ausf_e.assign_arpf_id(arpf_e.get_id());
        ausf_e.assign_amf_id(amf_e.get_id());
        arpf_e.assign_ausf_id(ausf_e.get_id());
        amf_e.assign_ausf_id(ausf_e.get_id());
        gNB_e.assign_amf_id(amf_e.get_id());
        gNB2_e.assign_amf_id(amf_e.get_id());
        gNB3_e.assign_amf_id(amf_e.get_id());
        // Setup initial gNB ID
        amf_e.assign_gnb_id(gNB_e.get_id());

        // Start the threads to handle each entity
        router_thread.th_ = std::thread{ msg_router };
        entity_threads_map[arpf_mh.get_id()].th_ = std::thread{ arpf_mh };
        entity_threads_map[ausf_mh.get_id()].th_ = std::thread{ ausf_mh };
        entity_threads_map[amf_mh.get_id()].th_ = std::thread{ amf_mh };
        entity_threads_map[gNB_mh.get_id()].th_ = std::thread{ gNB_mh };
        entity_threads_map[gNB2_mh.get_id()].th_ = std::thread{ gNB2_mh };
        entity_threads_map[gNB3_mh.get_id()].th_ = std::thread{ gNB3_mh };
        entity_threads_map[me_mh.get_id()].th_ = std::thread{ me_mh };

        // Send a command to the first UE
        Byte_buffer bb{
            me_attach_ID, me.get_id(), Ref_point::cmd, rc_attach, gNB_e.get_id()
        };
        entity_threads_map[me.get_id()].qptr_->push_message(bb);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

#ifndef MSG_TIMING
        std::cout << red << "==========================================\n";
        arpf_e.report(std::cout);
        std::cout << '\n';
        ausf_e.report(std::cout);
        std::cout << '\n';
        amf_e.report(std::cout);
        std::cout << '\n';
        gNB_e.report(std::cout);
        std::cout << '\n';
        gNB2_e.report(std::cout);
        std::cout << '\n';
        gNB3_e.report(std::cout);
        std::cout << '\n';
        me.report(std::cout);

        std::cout << "==========================================\n\n" << normal;
#endif
        // Use std::cerr so output can be re-directed to a file and these
        // messages don't appear
        std::cerr
          << "Enter h to do a handover or s to stop and then press return\n";
        char c = '\0';
        while (c != 'h' && c != 's') { std::cin >> c; }
        if (c == 'h') {
            Byte_buffer ho_data{
                gNB2_e.get_id(), me.get_id(), static_cast<Byte>(kcf)
            };// Target, ME, re-key or not
            bb = Byte_buffer{
                gnb_ho_n2_ID, gNB_e.get_id(), Ref_point::cmd, rc_n2_ho
            };
            bb += me.get_suci();
            bb += ho_data;
            entity_threads_map[gNB_e.get_id()].qptr_->push_message(bb);
            std::this_thread::sleep_for(std::chrono::milliseconds(20));

#ifndef MSG_TIMING
            std::cout << red << "==========================================\n";
            arpf_e.report(std::cout);
            std::cout << '\n';
            ausf_e.report(std::cout);
            std::cout << '\n';
            amf_e.report(std::cout);
            std::cout << '\n';
            gNB_e.report(std::cout);
            std::cout << '\n';
            gNB2_e.report(std::cout);
            std::cout << '\n';
            gNB3_e.report(std::cout);
            std::cout << '\n';
            me.report(std::cout);
            std::cout << "==========================================\n\n"
                      << normal;
#endif
            // Use std::cerr so output can be re-directed to a file and these
            // message don't appear
            std::cerr << "Enter h to do a second handover or s to stop and "
                         "then press return\n";
            c = '\0';
            while (c != 's' && c != 'h') { std::cin >> c; }

            if (c == 'h') {
                Byte_buffer ho_data2{
                    gNB3_e.get_id(), me.get_id(), static_cast<Byte>(kcf)
                };// Target, ME, re-key or not
                bb = Byte_buffer{
                    gnb_ho_n2_ID, gNB2_e.get_id(), Ref_point::cmd, rc_n2_ho
                };
                bb += me.get_suci();
                bb += ho_data2;
                entity_threads_map[gNB2_e.get_id()].qptr_->push_message(bb);
                std::this_thread::sleep_for(std::chrono::milliseconds(20));
#ifndef MSG_TIMING
                std::cout << red
                          << "==========================================\n";
                arpf_e.report(std::cout);
                std::cout << '\n';
                ausf_e.report(std::cout);
                std::cout << '\n';
                amf_e.report(std::cout);
                std::cout << '\n';
                gNB_e.report(std::cout);
                std::cout << '\n';
                gNB2_e.report(std::cout);
                std::cout << '\n';
                gNB3_e.report(std::cout);
                std::cout << '\n';
                me.report(std::cout);
                std::cout << "==========================================\n\n"
                          << normal;
#endif
                // Use std::cerr so output can be re-directed to a file and
                // these message don't appear
                std::cerr << "Enter s to stop and then press return\n";
                c = '\0';
                while (c != 's') { std::cin >> c; }
            }
        }
#ifdef MSG_TIMING
        ho_timings.write_timings(std::cout);
#endif
#ifdef MSG_COUNT
        for (const auto &mc : ho_msg_counts) {
            std::cout << mc.type
                      << (kcf == changed ? " re-keyed" : " not re-keyed")
                      << '\t' << mc.byte_count << '\n';
        }
#endif
#ifdef DEBUG
        locked_print(std::cout, "Stopping the Msg_handlers.\n");
#endif
        router_thread.qptr_->push_message(Byte_buffer{ terminate_ID, '\0' });
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        router_thread.th_.join();

        close_down_the_threads(entity_threads_map);

#ifdef DEBUG
        locked_print(std::cout, "Stopping the Msg_router.\n");
#endif
        EC_KEY_free(op_pub_key);
        EC_KEY_free(op_priv_key);
    } catch (const std::exception &e) {
        std::cerr << e.what() << "\n\n";
    }

    cleanup_openssl();

    return EXIT_SUCCESS;
}
