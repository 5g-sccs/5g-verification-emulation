/*******************************************************************************
 * File:        gNB_entity.cpp
 * Description: The entity representing gNodeB (the radio)
 *
 * Created:     Tuesday 20 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <sstream>
#include <iomanip>
#include "Msg_handler.h"
#include "Entity_shared_data.h"
#include "Hmac.h"
#include "Key_derivation.h"
#include "gNB_entity.h"
#include "Model_param.h"
#include "Msg_utils.h"

Byte_buffer generate_new_crnti()
{
    static uint16_t crnti_val(0x00);
    crnti_val = (++crnti_val > 0x960) ? 1 : crnti_val;
    return uint16_to_bb(crnti_val);
}

Msg_response gNB_entity::process_message(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << std::hex << "\ngNB " << 0 + id_ << " received:\n";
    debug_message(os, bb);
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Id_type source = bb[f_srce];
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
#ifdef DEBUG
    os << "gNB checking for SUCI: " << suci << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    auto const pos = gnb_fsms_.find(suci);
    if (pos != gnb_fsms_.end()) {
        auto res = pos->second->update_state(bb);
        if (bb[f_rc] != rc_xn_ho_comp && bb[f_rc] != rc_n2_ho_rel) {
            return res;
        }
#ifdef DEBUG
        os << "Erasing the gNB_fsm for gNB: " << 0 + bb[f_dest] << '\n';
        locked_print(std::cout, os.str());
        os.str("");
#endif
        gNB_fsm *fsm_ptr = pos->second;
        pos->second = nullptr;
        delete fsm_ptr;
        gnb_fsms_.erase(pos);
#ifdef DEBUG
        os << "The gNB now has " << gnb_fsms_.size() << " entries\n";
        locked_print(std::cout, os.str());
        os.str("");
#endif
        return res;
    }
    auto ref = static_cast<Ref_point>(bb[f_ref]);
    bool xn_handover_request = ref == Ref_point::xn && source >= gNB_base_ID
                               && source < amf_base_ID
                               && bb[f_rc] == rc_xn_ho_req;
    bool n2_handover_request = ref == Ref_point::n2 && source >= amf_base_ID
                               && source < ausf_base_ID
                               && bb[f_rc] == rc_n2_ho_req;
#ifdef DEBUG
    os << "source: " << 0 + source << " ref: " << 0 + bb[f_ref]
       << " rc: " << 0 + bb[f_rc] << '\n';
    os << std::boolalpha << "xn_handover_request: " << xn_handover_request
       << '\n';
    os << "n2_handover_request: " << n2_handover_request << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    if ((ref == Ref_point::rrc && source < gNB_base_ID) || xn_handover_request
        || n2_handover_request) {
        Id_type me_id = (xn_handover_request || n2_handover_request)
                          ? bb[hdr_size + suci_size]
                          : source;
        Byte_buffer crnti;
        crnti = generate_new_crnti();
#ifdef DEBUG
        locked_print(std::cout, "Creating a new gNB_fsm for the new ME\n");
#endif
        auto *fsm_ptr =
          new gNB_fsm(id_, me_id, amf_id_, suci, crnti, router_queue_ptr_);
        if (fsm_ptr == nullptr) {
            throw(std::runtime_error("Unable to allocate memory for gNB_fsm"));
        }
        gnb_fsms_.emplace(suci, fsm_ptr);
#ifdef DEBUG
        os << "New state machine created for UE: " << 0 + me_id << '\n';
        locked_print(std::cout, os.str());
#endif
        return fsm_ptr->update_state(bb);
    }
    // For the moment no other options are considered
    return std::nullopt;
}

gNB_entity::~gNB_entity()
{
    std::ostringstream os;
#ifdef DEBUG
    os << "gNB " << 0 + id_ << " destructor. Handling " << gnb_fsms_.size()
       << " MEs\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    for (auto &f : gnb_fsms_) {
#ifdef DEBUG
        os << "Deleting gNB_fsm for UE: " << f.first << '\n';
        locked_print(std::cout, os.str());
        os.str("");
#endif
        delete f.second;
    }
}

gNB_fsm::gNB_fsm(Id_type gnb_id,
  Id_type me_id,
  Id_type amf_id,
  Byte_buffer const &suci,
  Byte_buffer const &crnti,
  Msg_queue_ptr rq_ptr)
  : gnb_id_(gnb_id), me_id_(me_id), amf_id_(amf_id), suci_(suci), crnti_(crnti),
    router_queue_ptr_(rq_ptr)
{
    state_ptr_ = &gNB_fsm::active;
}

Msg_response gNB_fsm::update_state(Byte_buffer const &bb)
{
    Msg_response response = ((*this).*(state_ptr_))(bb);
    if (response != std::nullopt) {
        router_queue_ptr_->push_message(response.value());
    }

    return std::nullopt;// For now
}

Msg_response gNB_fsm::active(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(std::cout,
      "gNB: Active (message routing) state called for " + bb.to_hex_string()
        + '\n');
#endif

    Byte_buffer msg;
    auto ref = static_cast<Ref_point>(bb[f_ref]);
    switch (ref) {
    case Ref_point::n1:// Send on messages using the N1 reference point
        msg = bb;
        msg[f_dest] = (msg[f_srce] == me_id_) ? amf_id_ : me_id_;
#ifdef DEBUG
        os << "gNB forwarding message: " << bb << '\n';
        locked_print(std::cout, os.str());
#endif
        return msg;
        // break;
    case Ref_point::cmd:
        if (bb[f_dest] == gnb_id_) { return handle_cmd_message(bb); }
        return std::nullopt;
        // break;
    case Ref_point::n2:
        if (bb[f_dest] == gnb_id_) { return handle_n2_message(bb); }
        return std::nullopt;
        // break;
    case Ref_point::rrc:
        if (bb[f_dest] == gnb_id_) { return handle_rrc_message(bb); }
        return std::nullopt;
        // break;
    case Ref_point::xn:
        if (bb[f_dest] == gnb_id_) { return handle_xn_message(bb); }
        return std::nullopt;
        // break;
    default:
        return std::nullopt;
    }

    return std::nullopt;
}

Msg_response gNB_fsm::handle_rrc_message(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << "gNB handling rrc message: " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg;
    Byte rc = bb[f_rc];
    if (rc == rc_me_reg) {
        msg = Byte_buffer{ gnb_id_, me_id_, Ref_point::rrc, rc_crnti, amf_id_ };
        msg += suci_ + crnti_;
#ifdef PRINT
        locked_print(std::cout, g_to_m + "\nC-RNTI\n\n");
#endif
        return msg;
    }
    if (rc == rc_xn_ho_cfrm) {// && in_handover) {
        crnti_ = crnti_star_;
        k_gnb_ = k_gnb_star_;
#ifdef PRINT
        os << "\ngNB" << 0 + gnb_id_ << ": ME handover to new gNB ("
           << 0 + gnb_id_ << ") completed\n\n";
        locked_print(std::cout, os.str());
        os.str("");
#endif
#ifdef DEBUG
        os << "Sending completion message to AMF, should return NH, NCC pair\n";
        locked_print(std::cout, os.str());
        os.str("");
#endif
        msg = Byte_buffer{ gnb_id_, amf_id_, Ref_point::n2, rc_xn_pdu_id };
        msg += suci_;
        router_queue_ptr_->push_message(msg);

        msg = Byte_buffer{
            gnb_id_, gnb_ho_source_id_, Ref_point::xn, rc_xn_ho_comp
        };
        msg += suci_;

        return msg;
    }

    if (rc == rc_n2_ho_cfrm) {
#ifdef PRINT
        os << "\ngNB " << 0 + gnb_id_ << ": ME handover to new gNB ("
           << 0 + gnb_id_ << ") completed\n\n";
        os << "K_gNB: " << k_gnb_ << '\n';
        locked_print(std::cout, os.str());
        os.str("");
#endif
        msg = Byte_buffer{ gnb_id_, amf_id_, Ref_point::n2, rc_n2_ho_comp };
        msg += suci_;
#ifdef PRINT
        os << '\n' << g_to_a << "\n\n      gNB_id: " << 0 + gnb_id_ << "\n\n";
        locked_print(std::cout, os.str());
        os.str("");
#endif
#ifdef DEBUG
        os << "Sending N2 handover completion message to AMF\n";
        os << std::hex << "\ngNB " << 0 + gnb_id_ << " sent:\n";
        debug_message(os, msg);
        locked_print(std::cout, os.str());
        os.str("");
#endif
        return msg;
    }

    return std::nullopt;
}

Msg_response gNB_fsm::handle_n2_message(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << "gNB handling N2 message: " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg;
    if (bb[f_rc] == rc_auth_ack) {
        k_gnb_ = bb.get_part(hdr_size + suci_size, get_kdf_size(Kdf_id::gNB));
#ifdef PRINT
        os << "gNB: Authentication OK, received K_GNB from AMF\n";
        os << "\n K_GNB: " << k_gnb_ << "\n\n";
        locked_print(std::cout, os.str());
#endif
        return std::nullopt;
    }

    if (bb[f_rc] == rc_xn_pdu_cfrm) {
        assign_new_hop_data(
          unpack_next_hop_data(bb.get_part(hdr_size + suci_size, nhd_bb_size)));
#ifdef PRINT
        os << "gNB: Received a new NH, NCC pair from AMF\n";
        os << " NH: " << current_next_hop_data_.nhd_.nh_ << "\n";
        os << "NCC: " << current_next_hop_data_.nhd_.ncc_ << "\n";
        locked_print(std::cout, os.str());
#endif
        return std::nullopt;
    }

    if (bb[f_rc] == rc_n2_ho_req) {
        size_t offset = hdr_size + suci_size + 1;

        Next_hop_data nhd =
          unpack_next_hop_data(bb.get_part(offset, nhd_bb_size));
        assign_new_hop_data(
          nhd);//  Set current_next_hop_data_ to incoming value

        offset += nhd_bb_size;
        Nasc nasc = unpack_nasc(bb.get_part(offset, nasc_bb_size));
        if (nasc.amf_status_ == unchanged) {
            Byte_buffer kdf_S{ get_kdf_fc(Kdf_id::ng_ran_star_g) };
            Byte_buffer target_id{ gnb_id_ };
            kdf_S += params_to_kdf_string(
              target_id);// Just include the target ID number ??
            k_gnb_ = hmac_sha256(current_next_hop_data_.nhd_.nh_, kdf_S);
        } else if (nasc.amf_status_ == changed) {
            k_gnb_ = current_next_hop_data_.nhd_
                       .nh_;// This just contains the new K_gNB
        } else {
            locked_print(std::cout, "Invalid AMF key change status\n");
            return std::nullopt;
        }
        current_next_hop_data_.unused_ =
          false;// Either a new gNB, or we have just used it
#ifdef PRINT
        os << "New gNB: Received a handover request from AMF " << 0 + bb[f_srce]
           << "\n";
        os << "AMF key is " << (nasc.amf_status_ == unchanged ? "not " : "")
           << "re-keyed\n";
        os << "    gNB: Received an NH, NCC pair from AMF\n";
        os << "     NH: " << current_next_hop_data_.nhd_.nh_ << "\n";
        os << "    NCC: " << current_next_hop_data_.nhd_.ncc_ << "\n";
        os << "  K_GNB: " << k_gnb_ << "\n\n";

        locked_print(std::cout, os.str());
        os.str("");

        os << '\n'
           << g_to_a << "\n\n      gNB_id: " << 0 + gnb_id_
           << "\n      ME_id: " << 0 + me_id_ << "\n\n";
        locked_print(std::cout, os.str());
#endif
        msg = Byte_buffer{ gnb_id_, bb[f_srce], Ref_point::n2, rc_n2_ho_ack };
        msg += bb.get_part(hdr_size, suci_size);
        msg += pack_nasc(nasc);
        return msg;
    }

    if (bb[f_rc] == rc_n2_ho_cmd) {
        size_t offset = hdr_size + suci_size + 1;
        uint32_t ncc = bb_to_uint32(bb.get_part(offset, sizeof(uint32_t)));
        offset += sizeof(uint32_t);
        Byte_buffer nasc_bb = bb.get_part(offset, nasc_bb_size);
#ifdef PRINT
        Id_type target = bb[hdr_size + suci_size];
        os << "Old gNB: Received a handover command from AMF " << 0 + bb[f_srce]
           << "\n";
        os << '\n'
           << g_to_m << "\n\n      gNB_id: " << 0 + gnb_id_ << '\n'
           << "  New gNB_id: " << 0 + target << "\n\n";
        locked_print(std::cout, os.str());
#endif
        msg = Byte_buffer{ gnb_id_, me_id_, Ref_point::rrc, rc_n2_ho_cmd };
        msg += bb.get_part(hdr_size, suci_size + 1);// +1 for the target ID
        msg += uint32_to_bb(ncc);
        msg += nasc_bb;

        return msg;
    }

    if (bb[f_rc] == rc_n2_ho_rel) {
#ifdef PRINT
        os << "\nOld gNB " << 0 + gnb_id_
           << ": ME handover to new gNB completed - releasing UE\n\n";
        locked_print(std::cout, os.str());
        os.str("");
#endif
        msg = Byte_buffer{ gnb_id_, amf_id_, Ref_point::n2, rc_n2_ho_done };
        msg += suci_;
#ifdef PRINT
        os << '\n'
           << g_to_a << "\n\n      gNB_id: " << 0 + gnb_id_
           << " released ME\n\n";
        locked_print(std::cout, os.str());
#endif
        return msg;
    }

    return std::nullopt;
}

Msg_response gNB_fsm::handle_cmd_message(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << "gNB handling cmd message: " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg;

    if (bb[f_dest] != gnb_id_) { return std::nullopt; }
    Id_type source = bb[f_srce];
    switch (source) {
    case gnb_ho_xn_ID:
#ifdef DEBUG
        os << "gNB handling Xn handover message\n";
        locked_print(std::cout, os.str());
        os.str("");
#endif
        return trigger_xn_handover(bb);
    case gnb_ho_n2_ID:
#ifdef DEBUG
        os << "gNB handling N2 handover message\n";
        locked_print(std::cout, os.str());
        os.str("");
#endif
        return trigger_n2_handover(bb);
    case use_nh_ID:
#ifdef DEBUG
        os << "gNB marking NH data as being used\n";
        locked_print(std::cout, os.str());
        os.str("");
#endif
        current_next_hop_data_.unused_ = false;
    }
    return std::nullopt;
}

Msg_response gNB_fsm::trigger_xn_handover(Byte_buffer const &bb)
{
    std::ostringstream os;
    Byte_buffer msg;

    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
    Id_type me_id = bb[hdr_size + suci_size];
    if (me_id != me_id_) {
        locked_print(std::cout, "Inconsistent ME ids in handover request\n");
        return std::nullopt;
    }
#ifdef MSG_TIMING
    ho_timer.reset();
#endif
#ifdef MSG_COUNT
    ho_msg_count.in_handover = true;
    ho_msg_count.type = "Xn ho";
#endif
    gnb_id_star_ = bb[hdr_size + suci_size + 1];// New target gnb_id
#ifdef DEBUG
    os << std::hex << "gNB new id: " << 0 + gnb_id_star_ << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    // crnti_star_ = generate_new_crnti();
    Byte_buffer kdf_S{ get_kdf_fc(Kdf_id::ng_ran_star_g) };
    Byte_buffer target_id{ gnb_id_star_ };
    kdf_S +=
      params_to_kdf_string(target_id);// Just include the target ID number ??
    doing_vertical_ = current_next_hop_data_.unused_;
    k_gnb_star_ = (doing_vertical_)
                    ? hmac_sha256(current_next_hop_data_.nhd_.nh_, kdf_S)
                    : hmac_sha256(k_gnb_, kdf_S);
    current_next_hop_data_.unused_ = false;
#ifdef DEBUG
    os << "\n  kdf_S: " << kdf_S << "\n\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
#ifdef PRINT
    os << '\n' << g_to_g << "\n\n";
    os << "Doing a " << (doing_vertical_ ? "vertical" : "horizontal")
       << " handover to gNB_id*: " << 0 + gnb_id_star_ << "\n\n";
    os << " C-RNTI: " << crnti_ << '\n';
    os << " K_GNB*: " << k_gnb_star_ << "\n\n";
    locked_print(std::cout, os.str());
#endif
    Byte_buffer me_id_bb{ me_id_ };
    msg = Byte_buffer{ gnb_id_, gnb_id_star_, Ref_point::xn, rc_xn_ho_req };
    msg += suci_;
    msg += me_id_bb;
    msg += crnti_;
    msg += k_gnb_star_;
    msg += uint32_to_bb(current_next_hop_data_.nhd_.ncc_);

    in_handover = true;
    return msg;
}

Msg_response gNB_fsm::trigger_n2_handover(Byte_buffer const &bb)
{
    std::ostringstream os;
    Byte_buffer msg;

    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
    gnb_id_star_ = bb[hdr_size + suci_size];// New target gnb_id
#ifdef MSG_TIMING
    ho_timer.reset();
#endif
#ifdef MSG_COUNT
    ho_msg_count.in_handover = true;
    ho_msg_count.type = "N2 ho";
#endif
#ifdef DEBUG
    os << std::hex << "gNB new id: " << 0 + gnb_id_star_ << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    msg = Byte_buffer{ gnb_id_, amf_id_, Ref_point::n2, rc_n2_ho };
    msg += suci_;
    msg += bb.get_part(hdr_size + suci_size, 3);
    in_handover = true;
#ifdef PRINT
    Id_type me_id = bb[hdr_size + suci_size + 1];
    os << '\n' << g_to_a << "\n\n      gNB_id: " << 0 + gnb_id_ << '\n';
    os << "       ME id: " << 0 + me_id << '\n';
    os << "      AMF id: " << 0 + amf_id_ << '\n';
    os << "  New gNB_id: " << 0 + gnb_id_star_ << "\n\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    return msg;
}

Msg_response gNB_fsm::handle_xn_message(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << "gNB handling Xn message: " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg;
    Id_type source = bb[f_srce];
    Byte rc = bb[f_rc];
    if (bb[f_dest] == gnb_id_) {
        if (rc == rc_xn_ho_req) {// On target gNB
            Byte_buffer suci = bb.get_part(hdr_size, suci_size);
            crnti_star_ = bb.get_part(
              hdr_size + suci_size + 1, crnti_size);// C-RNTI from old gNB
            k_gnb_star_ = bb.get_part(hdr_size + suci_size + 1 + crnti_size,
              get_kdf_size(Kdf_id::ng_ran_star_g));
            gnb_ho_source_id_ = source;// Keep the old gNB id for later
#ifdef PRINT
            Id_type me_id = bb[hdr_size + suci_size];
            os << '\n' << g_to_g << "\n\ngNB_id*: " << 0 + gnb_id_ << '\n';
            os << "  ME ID:" << me_id;
            os << "C-RNTI*: " << crnti_ << '\n';// C-RNTI for target gNB
            os << " K_GNB*: " << k_gnb_star_ << "\n\n";
            locked_print(std::cout, os.str());
            os.str("");
#endif
            msg = Byte_buffer{ gnb_id_, source, Ref_point::xn, rc_xn_ho_acc };
            msg += suci_;
            msg += crnti_star_;// C-RNTI from old gNB
            msg += crnti_;// C-RNTI for this (new) gNB
            msg += bb.get_part(
              hdr_size + suci_size + 1 + crnti_size + get_kdf_size(Kdf_id::gNB),
              sizeof(uint32_t));// NCC
            in_handover = true;
        } else if (rc == rc_xn_ho_acc) {// On source gNB
            Byte_buffer target{ gnb_id_star_ };
            crnti_star_ =
              bb.get_part(hdr_size + suci_size + crnti_size, crnti_size);
            msg = Byte_buffer{ gnb_id_, me_id_, Ref_point::rrc, rc_xn_ho_me };
            msg += suci_;
            msg += target;
            msg += crnti_star_;
            msg += bb.get_part(
              hdr_size + suci_size + 2 * crnti_size, sizeof(uint32_t));// NCC
#ifdef DEBUG
            os << "\nSending message: " << msg << '\n';
            os << "    ME ID: " << me_id_ << '\n';
            os << "Target ID: " << target << '\n';
            os << "NCC: "
               << bb.get_part(
                    hdr_size + suci_size + 2 * crnti_size, sizeof(uint32_t))
               << '\n';
#endif
#ifdef PRINT
            os << '\n' << g_to_m << "\n\n";
            os << "C-RNTI*: " << crnti_star_ << "\n\n";// C-RNTI for target gNB
            locked_print(std::cout, os.str());
#endif
        } else if (rc == rc_xn_ho_comp) {// Nothing to do as we are going to
                                         // delete this entry on return

#ifdef MSG_TIMING
            auto dur = ho_timer.get_duration();
            os << "Xn ho " << (doing_vertical_ ? "vertical" : "horizontal");
            ho_timings.add(os.str(), dur);
            ho_timer.reset();
            os.str("");
#endif
#ifdef MSG_COUNT
            ho_msg_counts.push_back(ho_msg_count);
            reset_ho_msg_count();
#endif
#ifdef PRINT
            os << '\n'
               << g_to_g << "\n\nOld gNB (" << 0 + gnb_id_
               << ") received completion message from target gNB ("
               << 0 + source << ")\n\n";
            locked_print(std::cout, os.str());
#endif
            return std::nullopt;
        }

        return msg;
    }

    return std::nullopt;
}

void gNB_entity::report(std::ostream &os) const
{
    os << "Report for gNB: " << 0 + id_ << '\n';
    os << "gNB has " << gnb_fsms_.size() << " UEs attached\n";
}

void gNB_fsm::assign_new_hop_data(Next_hop_data const &nhd)
{
    current_next_hop_data_.nhd_.nh_ = nhd.nh_;
    current_next_hop_data_.nhd_.ncc_ = nhd.ncc_;
    current_next_hop_data_.unused_ = true;
}
