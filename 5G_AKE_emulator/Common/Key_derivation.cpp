/*******************************************************************************
 * File:        Key_derivation.cpp
 * Description: The key derivation functions
 *
 * Created:     Saturday 10 March 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include "Byte_buffer.h"
#include "Sha.h"
#include "Hmac.h"
#include "Entity_shared_data.h"
#include "Key_derivation.h"


// See 3GPP 33.501, 6.9.2.1.1 and Annex A.10
// The first pair is (NCC=0, NH=k_gNB)
Next_hop_data initialise_next_hop_data(Byte_buffer const &k_gnb)
{
    Next_hop_data nhd;
    nhd.nh_ = k_gnb;
    nhd.ncc_ = 0;

    return nhd;
}

// See 3GPP 33.501, 6.9.2.1.1 and Annex A.10
void update_next_hop_data(
  Byte_buffer const &k_amf, Next_hop_data &nhd, uint32_t new_ncc)
{
    Byte_buffer fc{ get_kdf_fc(Kdf_id::nh) };
    Byte_buffer kdf_S;
    while (nhd.ncc_ < new_ncc) {
        kdf_S = fc;
        kdf_S += params_to_kdf_string(nhd.nh_);

        nhd.nh_ = hmac_sha256(k_amf, kdf_S);
        nhd.ncc_ += 1;
    }
}

Byte_buffer pack_next_hop_data(Next_hop_data const &nhd)
{
    Byte_buffer nhd_bb = nhd.nh_;
    nhd_bb += uint32_to_bb(nhd.ncc_);

    return nhd_bb;
}


Next_hop_data unpack_next_hop_data(Byte_buffer const &nhd_bb)
{
    Next_hop_data nhd;
    nhd.nh_ = nhd_bb.get_part(0, get_kdf_size(Kdf_id::nh));
    nhd.ncc_ =
      bb_to_uint32(nhd_bb.get_part(get_kdf_size(Kdf_id::nh), sizeof(uint32_t)));

    return nhd;
}

Byte_buffer ecies_kdf(Byte_buffer const &shared_secret)
{
    Byte_buffer be_counter{ '\00', '\00', '\00', '\01' };

    Byte_buffer kdf = sha256_bb(shared_secret + be_counter);

    be_counter[3] = 2;
    kdf += sha256_bb(shared_secret + be_counter);

    return kdf.get_part(0, 48);
}
