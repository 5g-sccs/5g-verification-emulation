/*******************************************************************************
 * File:        Ecies_aes.cpp
 * Description: AES functions for ecies
 *
 * Created:     Saturday 17 March 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include "Byte_buffer.h"
#include "Get_random_bytes.h"
#include "Ecies_aes.h"

Byte_buffer initialise_iv(size_t size)
{
    auto seed = seed_from_clock();

    Byte_buffer rand_iv = get_random_bytes(size, seed);

    return rand_iv;
}

AES_KEY get_shared_aes_key(Byte_buffer const &shared_aes_bb)
{
    if (shared_aes_bb.size() != AES_BLOCK_SIZE) {
        std::cerr << "Incorrect length for AES key\n";
        exit(1);
    }
    AES_KEY key;
    if (AES_set_encrypt_key(shared_aes_bb.cdata(), 128, &key) != 0) {
        std::cerr << "Could not set encryption key.";
        exit(1);
    }

    return key;
}

// id here is either the SUPI, or the SUCI
Byte_buffer encrypt_id(Byte_buffer const &id,
  Byte_buffer const &aes_key_bb,
  Byte_buffer const &initial_iv)
{
    AES_KEY key = get_shared_aes_key(aes_key_bb);
    Byte_buffer iv = initial_iv;
    Byte_buffer ecount(AES_BLOCK_SIZE, 0);
    unsigned int num = 0;
    size_t enc_blocks = (id.size() + AES_BLOCK_SIZE - 1) / AES_BLOCK_SIZE;
    Byte_buffer enc_id(enc_blocks * AES_BLOCK_SIZE, 0);

    CRYPTO_ctr128_encrypt(id.cdata(),
      enc_id.data(),
      id.size(),
      &key,
      iv.data(),
      ecount.data(),
      &num,
      (block128_f)AES_encrypt);

    return enc_id;
}
