/*******************************************************************************
 * File:        AMF_entity.cpp
 * Description: Code for the AMF entity
 *
 * Created:     Thursday 21 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/

#include <sstream>
#include <iomanip>
#include "Key_derivation.h"
#include "Hmac.h"
#include "Entity_shared_data.h"
#include "Amf_entity.h"
#include "Msg_handler.h"
#include "Model_param.h"
#include "Msg_utils.h"
#include "Sha.h"

void AMF_entity::report(std::ostream &os) const
{
    os << "Report for AMF: " << 0 + id_ << '\n';
    os << "AMF is handling " << amf_fsms_.size() << " UEs\n";
}

Msg_response AMF_entity::process_message(Byte_buffer const &bb)
{
#ifdef DEBUG
    std::ostringstream os;
    os << std::hex << "\nAMF " << 0 + id_ << " received:\n";
    debug_message(os, bb);
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Id_type source = bb[f_srce];
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
#ifdef DEBUG
    os << "AMF checking for SUCI: " << suci << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    auto const pos = amf_fsms_.find(suci);
    if (pos != amf_fsms_.end()) { return pos->second->update_state(bb); }
    if (source < gNB_base_ID) {
        auto *fsm_ptr = new AMF_fsm(
          id_, ausf_id_, gnb_id_, router_queue_ptr_);// Messages routed via gNB
        if (fsm_ptr == nullptr) {
            throw(std::runtime_error("Unable to allocate memory for AMF_fsm"));
        }
        amf_fsms_.emplace(suci, fsm_ptr);
#ifdef DEBUG
        os << "New state machine created for UE: " << source << '\n';
        locked_print(std::cout, os.str());
#endif
        return fsm_ptr->update_state(bb);
    }
    // For the moment no other options are considered
    return std::nullopt;
}

AMF_entity::~AMF_entity()
{
    std::ostringstream os;
#ifdef DEBUG
    os << "AMF " << 0 + id_ << " destructor. Handling " << amf_fsms_.size()
       << " MEs\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    for (auto &f : amf_fsms_) {
#ifdef DEBUG
        os << "Deleting AMF_fsm for UE: " << f.first << '\n';
        locked_print(std::cout, os.str());
        os.str("");
#endif
        delete f.second;
    }
}

AMF_fsm::AMF_fsm(
  Id_type amf_id, Id_type ausf_id, Id_type gnb_id, Msg_queue_ptr rq_ptr)
  : amf_id_(amf_id), gnb_id_(gnb_id), ausf_id_(ausf_id),
    router_queue_ptr_(rq_ptr)
{
    if (router_queue_ptr_ == nullptr) {
        throw std::runtime_error(
          "ASM_fsm incorrectly initialised: router_queue_ptr must be "
          "initialised");
    }
    ue_data_.amf_ue_common_.ksi_ = ksi_unset;
    state_ptr_ = &AMF_fsm::idle;
}

Msg_response AMF_fsm::update_state(Byte_buffer const &bb)
{
    Msg_response response = ((*this).*(state_ptr_))(bb);
    if (response != std::nullopt) {
        router_queue_ptr_->push_message(response.value());
    }

    return std::nullopt;// For now
}

Msg_response AMF_fsm::idle(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(
      std::cout, "AMF: Idle state called for " + bb.to_hex_string() + '\n');
#endif
    if (bb[f_rc] != rc_me_areg) {
        locked_print(std::cout, "AMF: unexpected reason code in idle state\n");
        return std::nullopt;
    }
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
    Byte_buffer msg{ amf_id_, gnb_id_, Ref_point::n1, rc_sn_id };
    msg += suci;
    msg += d_sn_name;
#ifdef PRINT
    locked_print(std::cout,
      s_to_m + "\nSending SN id\n\n SN id: " + d_sn_name.to_hex_string()
        + "\n\n");
#endif
    state_ptr_ = &AMF_fsm::awaiting_request;
    return msg;
}

Msg_response AMF_fsm::awaiting_request(Byte_buffer const &bb)
{
#ifdef DEBUG
    locked_print(std::cout,
      "AMF: State awaiting_request called for " + bb.to_hex_string() + '\n');
#endif
    if (bb[f_rc] != rc_me_auth_init) {
        locked_print(
          std::cout, "AMF: unexpected reason code while awaiting_request\n");
        return std::nullopt;
    }
    suci_ = bb.get_part(hdr_size, suci_size);
    size_t ecies_size = bb.size() - hdr_size - suci_size;

    Byte_buffer msg{ amf_id_, ausf_id_, Ref_point::n12, rc_auth_req };
    msg += suci_ + d_sn_name + bb.get_part(hdr_size + suci_size, ecies_size);

#ifdef PRINT
    size_t key_length = ecies_size - iv_size - mac_tag_size;
    locked_print(std::cout,
      s_to_a + "\n" + as_space
        + "Nausf_UEAuthentication_Authenticate Request\n\n  SUCI: "
        + suci_.to_hex_string() + "\n SN id: " + d_sn_name.to_hex_string()
        + "\nEP_KEY: "
        + bb.get_part(hdr_size + suci_size, key_length).to_hex_string()
        + "\n    IV: "
        + bb.get_part(hdr_size + suci_size + key_length, iv_size)
            .to_hex_string()
        + "\n   MAC: "
        + bb.get_part(hdr_size + suci_size + key_length + iv_size, mac_tag_size)
            .to_hex_string()
        + "\n\n");
#endif
    state_ptr_ = &AMF_fsm::awaiting_AV;
    return msg;
}

Msg_response AMF_fsm::awaiting_AV(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(std::cout,
      "AMF: State awaiting_AV called for " + bb.to_hex_string() + '\n');
#endif
    if (bb[f_rc] != rc_auth_resp) {
        locked_print(std::cout, "AMF: unexpected reason code in awaiting_AV\n");
        return std::nullopt;
    }
    suci_ = bb.get_part(hdr_size, suci_size);
    Byte_buffer supi = bb.get_part(hdr_size + suci_size, supi_size);
    Byte offset = hdr_size + suci_size + supi_size;
    rand_ = bb.get_part(offset, rand_size);
    offset += rand_size + autn_size;
    hxres_star_ = bb.get_part(offset, get_kdf_size(Kdf_id::hxres_star));
    // offset += get_kdf_size(Kdf_id::hxres_star);
#ifdef DEBUG
    os << "AMF: XRES*: " << hxres_star_.to_hex_string() << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
#ifdef PRINT
    os << s_to_m << "\nAuthentication Request\n\n  RAND: " << rand_
       << "\n  AUTN: ";
    os << bb.get_part(hdr_size + suci_size + supi_size + rand_size, autn_size)
       << "\n\n";
    locked_print(std::cout, os.str());
#endif
    Byte_buffer msg{ amf_id_, gnb_id_, Ref_point::n1, rc_me_auth };
    msg += suci_;
    msg += bb.get_part(hdr_size + suci_size + supi_size, rand_size + autn_size);
    state_ptr_ = &AMF_fsm::awaiting_me_response;
    return msg;
}

Msg_response AMF_fsm::awaiting_me_response(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(std::cout,
      "AMF: State awaiting_me_response called for " + bb.to_hex_string()
        + '\n');
#endif
    if (bb[f_rc] != rc_me_auth_resp) {
        locked_print(
          std::cout, "AMF: unexpected reason code in awaiting_me_resonse\n");
        return std::nullopt;
    }
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
    Byte_buffer res_star =
      bb.get_part(hdr_size + suci_size, get_kdf_size(Kdf_id::res_star));
#ifdef DEBUG
    os << "AMF: RES*: " << res_star << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer S = rand_ + res_star;
    Byte_buffer hres_star = sha256_bb(S).get_part(16, 16);

#ifdef DEBUG
    os << "AMF: HRES*: " << hres_star.to_hex_string() << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    bool msg_OK = false;
    Byte_buffer msg;
    if (hres_star == hxres_star_) {
        msg =
          Byte_buffer{ amf_id_, ausf_id_, Ref_point::n12, rc_amf_auth_resp };
        msg += suci;
        msg += res_star;
        msg_OK = true;
#ifdef PRINT
        os << s_to_a << "\n"
           << as_space
           << "Nausf_UEAuthentication_Authenticate Request\n\n  RES*: ";
        os << res_star << "\n\n";
        locked_print(std::cout, os.str());
#endif
        state_ptr_ = &AMF_fsm::awaiting_confirmation;
    } else {
        locked_print(std::cout, "Authentication unsuccessful\n\n");
    }

    if (msg_OK) { return msg; }

    return std::nullopt;
}

Msg_response AMF_fsm::awaiting_confirmation(Byte_buffer const &bb)
{
    std::ostringstream os;

    if (bb[f_rc] != rc_ausf_auth_ack) {
        locked_print(
          std::cout, "AMF: unexpected reason code in awaiting_confirmation\n");
        return std::nullopt;
    }
#ifdef PRINT
    locked_print(std::cout, "AMF: Authentication successful\n\n");
#endif
    ue_data_.amf_ue_common_.authenticated_ok = true;
    ue_data_.amf_ue_common_.uplink_nas_count_ = 0;
    ue_data_.amf_ue_common_.downlink_nas_count_ = 0;
    ue_data_.amf_ue_common_.supi_ =
      bb.get_part(hdr_size + suci_size, supi_size);
    ue_data_.amf_ue_common_.k_seaf_ =
      bb.get_part(hdr_size + suci_size + supi_size, get_kdf_size(Kdf_id::seaf));
    ue_data_.amf_ue_common_.abba_ = abba_;
    ue_data_.amf_ue_common_.imsi_ =
      ue_data_.amf_ue_common_.supi_;// IMSI the same as SUPI?????

    Byte_buffer kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::amf) };
    kdf_S += params_to_kdf_string(
      ue_data_.amf_ue_common_.imsi_, ue_data_.amf_ue_common_.abba_);
    ue_data_.amf_ue_common_.k_amf_ =
      hmac_sha256(ue_data_.amf_ue_common_.k_seaf_, kdf_S);

#ifdef DEBUG
    os << "[SUPI]: " << ue_data_.amf_ue_common_.supi_
       << "\nK_SEAF: " << ue_data_.amf_ue_common_.k_seaf_ << '\n';
    os << "  ABBA: " << ue_data_.amf_ue_common_.abba_ << '\n';
    os << " K_AMF: " << ue_data_.amf_ue_common_.k_amf_ << "\n\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif

    Byte_buffer at{ static_cast<Byte>(Kdf_access_type::three_gpp) };
    kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::gNB) };
    kdf_S += params_to_kdf_string(
      uint32_to_bb(ue_data_.amf_ue_common_.uplink_nas_count_), at);
    Byte_buffer k_gnb = hmac_sha256(ue_data_.amf_ue_common_.k_amf_, kdf_S);

    ue_data_.current_nhd_ = initialise_next_hop_data(k_gnb);

#ifdef DEBUG
    os << "\n  K_GNB: kdf_S: " << kdf_S << '\n';
    os << "    AMF: K_GNB: " << k_gnb << "\n\n";
    os << "    AMF:   NCC: " << ue_data_.current_nhd_.ncc_ << "\n";
    os << "    AMF:    NH: " << ue_data_.current_nhd_.nh_ << "\n\n";

    locked_print(std::cout, os.str());
    os.str("");
#endif
#ifdef PRINT
    os << a_to_g << "\n\n";
    os << "  K_GNB: " << k_gnb << "\n";
    os << "    NCC: " << ue_data_.current_nhd_.ncc_ << "\n";
    os << "     NH: " << ue_data_.current_nhd_.nh_ << "\n\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg{ amf_id_, gnb_id_, Ref_point::n2, rc_auth_ack };
    msg += suci_;
    msg += k_gnb;
    router_queue_ptr_->push_message(msg);
#ifdef PRINT
    os << s_to_m << "\n\n";
    os << "   KSI: " << 0 + ue_data_.amf_ue_common_.ksi_ << '\n';
    os << "  ABBA: " << ue_data_.amf_ue_common_.abba_ << "\n\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer ksi{ ue_data_.amf_ue_common_.ksi_ };
    msg = Byte_buffer{ amf_id_, gnb_id_, Ref_point::n1, rc_auth_ack };
    msg += suci_;
    msg += ksi;
    msg += ue_data_.amf_ue_common_.abba_;

    state_ptr_ = &AMF_fsm::active;

    return msg;
}

Msg_response AMF_fsm::active(Byte_buffer const &bb)
{
    std::ostringstream os;
    Byte_buffer msg;
    auto ref = static_cast<Ref_point>(bb[f_ref]);

#ifdef DEBUG
    os << "AMF: State active called for " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    if (bb[f_rc] == rc_xn_pdu_id) {
#ifdef PRINT
        locked_print(std::cout,
          "AMF: received handover completion, sending NH,NCC pair\n");
#endif
        Id_type source = bb[f_srce];

        uint32_t new_ncc = ue_data_.current_nhd_.ncc_ + 1;
        update_next_hop_data(
          ue_data_.amf_ue_common_.k_amf_, ue_data_.current_nhd_, new_ncc);

        msg = Byte_buffer{ amf_id_, source, Ref_point::n2, rc_xn_pdu_cfrm };
        msg += suci_;
        msg += pack_next_hop_data(ue_data_.current_nhd_);

        return msg;
    }

    if (bb[f_rc] == rc_n2_ho && ref == Ref_point::n2) {
#ifdef PRINT
        locked_print(std::cout, "AMF: received N2 handover request\n");
#endif
#ifdef DEBUG
        os << "          SUCI: " << suci_ << '\n';
        os << "K_AMF on entry: " << ue_data_.amf_ue_common_.k_amf_ << '\n';
        locked_print(std::cout, os.str());
        os.str("");
#endif
        srce_in_handover_ = bb[f_srce];
        target_in_handover_ = bb[hdr_size + suci_size];
        me_in_handover_ = bb[hdr_size + suci_size + 1];

        amf_change_flag_ =
          static_cast<Nasc_kamf_change_flag>(bb[hdr_size + suci_size + 2]);
#ifdef MSG_TIMING
        os << "N2 ho "
           << (amf_change_flag_ == changed ? "re-keyed" : "not re-keyed");
        ho_type_ = os.str();
        os.str("");
#endif
        Nasc nasc;
        nasc.amf_status_ = amf_change_flag_;
        nasc.ksi_ = ue_data_.amf_ue_common_.ksi_;
        nasc.downlink_nas_count_ = ue_data_.amf_ue_common_.downlink_nas_count_;

        if (nasc.amf_status_ == unchanged) {
            uint32_t new_ncc = ue_data_.current_nhd_.ncc_ + 1;
            update_next_hop_data(
              ue_data_.amf_ue_common_.k_amf_, ue_data_.current_nhd_, new_ncc);
        } else if (nasc.amf_status_ == changed) {
            Byte_buffer kdf_S{ get_kdf_fc(Kdf_id::amf_prime),
                static_cast<Byte>(Amf_direction::ho) };
            kdf_S += params_to_kdf_string(
              uint32_to_bb(ue_data_.amf_ue_common_.downlink_nas_count_));
            k_amf_prime_ = hmac_sha256(ue_data_.amf_ue_common_.k_amf_, kdf_S);
#ifdef DEBUG
            os << " kdf_S: " << kdf_S << '\n';
            os << "K_AMF': " << k_amf_prime_ << '\n';
            locked_print(std::cout, os.str());
            os.str("");
#endif
            Byte_buffer at{ static_cast<Byte>(Kdf_access_type::three_gpp) };
            kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::gNB) };
            kdf_S += params_to_kdf_string(uint32_to_bb(rekey_nas_count), at);
            new_k_gnb_ = hmac_sha256(k_amf_prime_, kdf_S);
#ifdef DEBUG
            os << " kdf_S: " << kdf_S << '\n';
            os << "K_gNB': " << new_k_gnb_ << '\n';
            locked_print(std::cout, os.str());
            os.str("");
#endif
            new_nhd_ = initialise_next_hop_data(new_k_gnb_);
            // ue_data_.amf_ue_common_.ksi_ + 1;//  Just increment the value for
            // now, if it gets to "unset"(7) set to 0 ksi = (ksi == ksi_unset) ?
            // 0 : ksi;
        } else {
            locked_print(std::cout, "Invalid AMF key change status\n");
            return std::nullopt;
        }

        msg = Byte_buffer{
            amf_id_, target_in_handover_, Ref_point::n2, rc_n2_ho_req
        };
        msg += suci_;
        msg += bb.get_part(hdr_size + suci_size + 1, 1);// ME in handover
        msg += (nasc.amf_status_ == unchanged)
                 ? pack_next_hop_data(ue_data_.current_nhd_)
                 : pack_next_hop_data(new_nhd_);
        msg += pack_nasc(nasc);
#ifdef PRINT
        os << '\n' << a_to_g << "\n\n";
        os << "AMF: doing an N2 handover to gNB_id: " << 0 + target_in_handover_
           << " for ME_id: " << 0 + me_in_handover_ << ", AMF key is "
           << (nasc.amf_status_ == unchanged ? "not " : "") << "re-keyed\n\n";
        locked_print(std::cout, os.str());
#endif

        return msg;
    }

    if (bb[f_rc] == rc_n2_ho_ack && ref == Ref_point::n2
        && bb[f_srce] == target_in_handover_) {

#ifdef PRINT
        locked_print(std::cout,
          "AMF: received handover acknowledgement from target gNB\n");
#endif
        Nasc nasc =
          unpack_nasc(bb.get_part(hdr_size + suci_size, nasc_bb_size));

        Byte_buffer target_nb{ target_in_handover_ };
        msg = Byte_buffer{
            amf_id_, srce_in_handover_, Ref_point::n2, rc_n2_ho_cmd
        };
        msg += suci_;
        msg += target_nb;
        msg += uint32_to_bb(ue_data_.current_nhd_.ncc_);
        msg += pack_nasc(nasc);
#ifdef PRINT
        os << '\n' << a_to_g << "\n\n";
        os << "AMF: N2 handover command to gNB_id: " << 0 + srce_in_handover_
           << " for ME_id: " << 0 + me_in_handover_ << "\n\n";
        locked_print(std::cout, os.str());
#endif
        return msg;
    }

    if (bb[f_rc] == rc_n2_ho_comp && ref == Ref_point::n2
        && bb[f_srce] == target_in_handover_) {
#ifdef PRINT
        locked_print(
          std::cout, "AMF: received handover completion from target gNB\n");
#endif
        msg = Byte_buffer{
            amf_id_, srce_in_handover_, Ref_point::n2, rc_n2_ho_rel
        };
        msg += suci_;

#ifdef PRINT
        os << '\n' << a_to_g << "\n\n";
        os << "AMF: N2 handover to gNB_id: " << 0 + target_in_handover_
           << " for ME_id: " << 0 + me_in_handover_ << " completed\n\n";
        locked_print(std::cout, os.str());
#endif
        if (amf_change_flag_ == changed) {
            ue_data_.current_nhd_ = new_nhd_;
            update_next_hop_data(k_amf_prime_, ue_data_.current_nhd_, 1);//
            ue_data_.amf_ue_common_.k_amf_ = k_amf_prime_;
            ue_data_.amf_ue_common_.uplink_nas_count_ = 0;
            ue_data_.amf_ue_common_.downlink_nas_count_ = 0;// Check this !!!!
        }

        new_nhd_ = Next_hop_data{};
        target_in_handover_ = unset_ID;
        me_in_handover_ = unset_ID;
        amf_change_flag_ = flag_unset;
        k_amf_prime_.clear();
        new_k_gnb_.clear();

        return msg;
    }

    if (bb[f_rc] == rc_n2_ho_done && ref == Ref_point::n2
        && bb[f_srce] == srce_in_handover_) {
#ifdef PRINT
        locked_print(
          std::cout, "AMF: received released message from source gNB\n");
#endif
        srce_in_handover_ = unset_ID;
#ifdef MSG_TIMING
        auto dur = ho_timer.get_duration();
        ho_timings.add(ho_type_, dur);
        ho_timer.reset();
        ho_type_.clear();
#endif
#ifdef MSG_COUNT
        ho_msg_counts.push_back(ho_msg_count);
        reset_ho_msg_count();
#endif
        return std::nullopt;
    }

    locked_print(std::cout, "AMF: unable to handle the request\n");
    return std::nullopt;
}
