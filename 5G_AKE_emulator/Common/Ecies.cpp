/*******************************************************************************
 * File:        Ecies.cpp
 * Description: The functions for ecies
 *
 * Created:     Saturday 17 March 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <iostream>
#include <openssl/evp.h>

#include "Ecies.h"
#include "Ecies_utils.h"

EC_KEY *ue_generate_eph_keys(EC_KEY *op_pub_key)
{
    const EC_GROUP *ec_group = nullptr;
    ec_group = EC_KEY_get0_group(op_pub_key);
    int curve;
    curve = EC_GROUP_get_curve_name(ec_group);

    //	std::cout << "Curve ID: " << OBJ_nid2sn(curve) << "(" << curve << ")\n";

    EC_KEY *eph_ec_key = nullptr;
    eph_ec_key = EC_KEY_new_by_curve_name(curve);
    int kres = EC_KEY_generate_key(eph_ec_key);
    if (kres == 0) {
        std::cerr << "Unable to generate the ephemeral keys\n";
        return nullptr;
    }
    //		std::cout << "Ephemeral keys generated\n";

    return eph_ec_key;
}

std::pair<Byte_buffer, Byte_buffer> ue_generate_eph_data(EC_KEY *op_pub_key)
{
    EC_KEY *eph_ec_key = ue_generate_eph_keys(op_pub_key);
    if (eph_ec_key == nullptr) {
        std::cout << "Generating the ephemeral key failed\n";
        exit(0);
    }

    EVP_PKEY *tmp_pkey = EVP_PKEY_new();
    if (EVP_PKEY_assign_EC_KEY(tmp_pkey, eph_ec_key) == 0) {
        std::cerr << "Unable to create EVP_PKEY\n";
        exit(0);
    }
    //		std::cout << "EVP_PKEY structure created\n";

    EC_KEY *eph_pub_key = EVP_PKEY_get1_EC_KEY(tmp_pkey);

    Byte_buffer eph_pub_bb = public_key_to_byte_buffer(eph_pub_key);

    //	std::cout << "Now generate the shared secret\n";

    int field_degree;
    uint32_t field_size;
    uint32_t field_bytes;
    int secret_len;

    field_degree = EC_GROUP_get_degree(EC_KEY_get0_group(eph_ec_key));
    if (field_degree == 0) {
        std::cerr << "EC_GROUP_get_degree failed\n";
        exit(0);
    }

    field_size = static_cast<uint32_t>(field_degree);// Number of bits
    field_bytes = (field_size + 7) / 8;
    //	std::cout << "Length of shared key " << secret_len << '\n';

    Byte_buffer eph_shared_data(field_bytes, 0);

    /* Derive the shared secret */
    secret_len = ECDH_compute_key(&eph_shared_data[0],
      field_bytes,
      EC_KEY_get0_public_key(op_pub_key),
      eph_ec_key,
      nullptr);

    if (secret_len <= 0) {
        std::cerr << "Creation of the shared key failed\n";
        // OPENSSL_free(secret);
        exit(0);
    }
    //		std::cout << "Shared key created\n"
    //				  << eph_shared_data.to_hex_string() << '\n';
    //		std::cout << "Shared key length " << eph_shared_data.length() <<
    //'\n';

    EC_KEY_free(eph_ec_key);
    EC_KEY_free(eph_pub_key);

    return std::make_pair(eph_pub_bb, eph_shared_data);
}

Byte_buffer op_generate_eph_data(
  EC_KEY *op_priv_key, Byte_buffer const &eph_pub_bb)
{
    const EC_GROUP *ec_group = nullptr;
    ec_group = EC_KEY_get0_group(op_priv_key);

    //	int curve;
    //	curve = EC_GROUP_get_curve_name(ec_group);
    //	std::cout << "Curve ID: " << OBJ_nid2sn(curve) << "(" << curve << ")\n";

    int field_degree;
    uint32_t field_size;
    uint32_t field_bytes;

    field_degree = EC_GROUP_get_degree(EC_KEY_get0_group(op_priv_key));
    if (field_degree == 0) {
        std::cerr << "Unable to obtain a degree for the field\n";
        exit(0);
    }
    field_size = static_cast<uint32_t>(field_degree);// Number of bits
    field_bytes = (field_size + 7) / 8;// Number of bytes
    //	std::cout << std::dec << "Length of shared key " << secret_len << '\n';

    Byte_buffer eph_shared_data_op(field_bytes, 0);

    int secret_len;

    //	std::cout << "Converting the byte buffer to an EC_point. Byte buffer
    // size " << eph_pub_bb.length() << '\n';
    EC_POINT *pubk_point = nullptr;
    public_key_byte_buffer_to_point(ec_group, eph_pub_bb, &pubk_point);

    //	std::cout << "Obtained ephemeral public key from the buffer\n";

    /* Derive the shared secret */
    secret_len = ECDH_compute_key(
      &eph_shared_data_op[0], field_bytes, pubk_point, op_priv_key, nullptr);
    if (secret_len <= 0) {
        std::cerr << "Creation of the shared key failed\n";
        exit(0);
    }
    //		std::cout << "Shared key created\n"
    //				  << eph_shared_data_op.to_hex_string() << '\n';

    return eph_shared_data_op;
}
