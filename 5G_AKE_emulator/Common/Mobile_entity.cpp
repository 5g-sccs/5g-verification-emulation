/*******************************************************************************
 * File:        Mobile_entity.cpp
 * Description: Code for the mobile entity class
 *
 * Created:     Thursday 21 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <sstream>
#include <iomanip>
#include "Hmac.h"
#include "Key_derivation.h"
#include "Authentication_utils.h"
#include "Mobile_entity.h"
#include "Model_param.h"
#include "Msg_utils.h"

Mobile_entity::~Mobile_entity()
{
#ifdef DEBUG
    std::ostringstream os;
    os << "ME " << 0 + id_ << " destructor\n";
    locked_print(std::cout, os.str());
#endif
}

void Mobile_entity::report(std::ostream &os) const
{
    os << "Report for UE: " << 0 + id_ << '\n';
    os << "UE is connected to AMF " << 0 + amf_id_ << " via gNB " << 0 + gnb_id_
       << '\n';
}

void Mobile_entity::initialise()
{
    ecies_data_ = usim_.get_ecies_data();
#ifdef DEBUG
    std::ostringstream os;
    os << "SUPI: " << usim_.get_supi() << '\n';
    os << "SUCI: " << ecies_data_[0] << '\n';
    os << "Ephemeral public key+IV: " << ecies_data_[1] << '\n';
    os << "Hash: " << ecies_data_[2] << '\n';
    locked_print(std::cout, os.str());
#endif
    state_ptr_ = &Mobile_entity::idle;
}

Msg_response Mobile_entity::process_message(Byte_buffer const &bb)
{
#ifdef DEBUG
    std::ostringstream os;
    os << std::hex << "\nME " << 0 + id_ << " received:\n";
    debug_message(os, bb);
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Msg_response response = ((*this).*(state_ptr_))(bb);
    if (response != std::nullopt) {
        router_queue_ptr_->push_message(response.value());
    }

    return std::nullopt;// For now
}

Msg_response Mobile_entity::idle(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(std::cout,
      "ME: Idle state called with input " + bb.to_hex_string() + '\n');
#endif
    if (bb[f_rc] != rc_attach) {
        locked_print(std::cout, "ME: unexpected reason code in idle state\n");
        return std::nullopt;
    }
    gnb_id_ = bb[hdr_size];// Sent in the command
#ifdef DEBUG
    os << "ME " << 0 + id_ << ": gNB_id: " << 0 + gnb_id_ << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg{ id_, gnb_id_, Ref_point::rrc, rc_me_reg };
    msg += ecies_data_[0];// Add in the SUCI
#ifdef DEBUG
    os << "ME: Idle state: sending message: " << msg << '\n';
    locked_print(std::cout, os.str());
#endif
#ifdef PRINT
    locked_print(std::cout, m_to_g + "\nAttach\n\n");
#endif
    state_ptr_ = &Mobile_entity::waiting_crnti;
    return msg;
}

Msg_response Mobile_entity::waiting_crnti(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(std::cout,
      "ME: Waiting_crnti called with input " + bb.to_hex_string() + '\n');
#endif
    if (bb[f_rc] != rc_crnti) {
        locked_print(std::cout, "ME: unexpected reason code in idle state\n");
        return std::nullopt;
    }
    amf_id_ = bb[hdr_size];
    crnti_ = bb.get_part(hdr_size + suci_size + 1, crnti_size);
#ifdef DEBUG
    os << "ME " << 0 + id_ << ": amf_id: " << 0 + amf_id_ << '\n';
    locked_print(std::cout, os.str());
    os.str("");
    os << "ME " << 0 + id_ << ": C-RNTI: " << crnti_ << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg{ id_, gnb_id_, Ref_point::n1, rc_me_areg };
    msg += ecies_data_[0];// Add in the SUCI
#ifdef DEBUG
    os << "ME: Waiting_crnti: sending message: " << msg << '\n';
    locked_print(std::cout, os.str());
#endif
#ifdef PRINT
    locked_print(std::cout, m_to_s + "\nAttach\n\n");
#endif
    state_ptr_ = &Mobile_entity::waiting_sn_id;
    return msg;
}

Msg_response Mobile_entity::waiting_sn_id(Byte_buffer const &bb)
{
#ifdef DEBUG
    locked_print(std::cout,
      "ME: State waiting_sn_id called with input " + bb.to_hex_string() + '\n');
#endif
    if (bb[f_rc] == rc_sn_id) {
        sn_name_ = bb.get_part(hdr_size + suci_size, sn_name_size);
    } else {
        locked_print(
          std::cout, "ME: unexpected reason code in waiting_sn_id state\n");
        return std::nullopt;
    }
    Byte_buffer ue_mac =
      hmac_sha256(ecies_data_[2], ecies_data_[0] + ecies_data_[1])
        .get_part(0, 8);
    Byte_buffer msg{ id_, gnb_id_, Ref_point::n1, rc_me_auth_init };
    msg += ecies_data_[0] + ecies_data_[1] + ue_mac;
#ifdef PRINT
    size_t key_length = ecies_data_[1].size() - AES_BLOCK_SIZE;
    locked_print(std::cout,
      m_to_s + "\nRegistration Request\n\n  SUPI: "
        + usim_.get_supi().to_hex_string()
        + "\n\n  SUCI: " + ecies_data_[0].to_hex_string() + "\nEP_KEY: "
        + ecies_data_[1].get_part(0, key_length).to_hex_string() + "\n    IV: "
        + ecies_data_[1].get_part(key_length, AES_BLOCK_SIZE).to_hex_string()
        + "\n   MAC: " + ue_mac.to_hex_string() + "\n\n");
#endif
    state_ptr_ = &Mobile_entity::waiting_auth_response;
    return msg;
}

Msg_response Mobile_entity::waiting_auth_response(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(std::cout,
      "ME: State waiting_auth_response called for " + bb.to_hex_string()
        + '\n');
#endif
    if (bb[f_rc] != rc_me_auth) {
        locked_print(
          std::cout, "ME: unexpected reason code waiting for auth_response\n");
        return std::nullopt;
    }
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
#ifdef DEBUG
    locked_print(std::cout,
      "ME: waiting_auth_response called for SUCI: " + suci.to_hex_string()
        + '\n');
    locked_print(
      std::cout, "Stored SUCI: " + ecies_data_[0].to_hex_string() + '\n');
#endif
    Usim::Auth_result ar = usim_.authenticate(
      bb.get_part(hdr_size + suci_size, rand_size + autn_size));
    // Auth_result has RES, CK and IK
    Byte_buffer res = ar[0];
    Byte_buffer kdf_key = ar[1] + ar[2];
    Byte sqn_offset = hdr_size + suci_size + rand_size;
    Byte_buffer c_sqn = bb.get_part(sqn_offset, sqn_size);

    Byte_buffer kdf_S{ get_kdf_fc(Kdf_id::ausf) };
    kdf_S += params_to_kdf_string(sn_name_, c_sqn);
    Byte_buffer k_ausf = hmac_sha256(kdf_key, kdf_S);
#ifdef DEBUG
    os << "ME" << 0 + id_ << ": K_AUSF: " << k_ausf << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::seaf) };
    kdf_S += params_to_kdf_string(sn_name_);
    amf_data_.k_seaf_ = hmac_sha256(k_ausf, kdf_S);
#ifdef DEBUG
    os << "ME" << 0 + id_ << ": K_SEAF: " << amf_data_.k_seaf_ << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer rand = bb.get_part(hdr_size + suci_size, rand_size);
#ifdef DEBUG
    os << "ME" << 0 + id_ << ": RAND: " << rand.to_hex_string() << '\n';
    locked_print(std::cout, os.str());
    os.str("");
    os << "ME" << 0 + id_ << ": RES: " << res.to_hex_string() << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::res_star) };
    kdf_S += params_to_kdf_string(sn_name_, rand, res);
    Byte_buffer res_star = hmac_sha256(kdf_key, kdf_S).get_part(16, 16);
#ifdef DEBUG
    os << "ME" << 0 + id_ << ": RES*: " << res_star.to_hex_string() << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg{ id_, gnb_id_, Ref_point::n1, rc_me_auth_resp };
    msg += ecies_data_[0];// Add in the suci
    msg += res_star;
#ifdef PRINT
    os << m_to_s << "\nAuthentication Response\n\n  RES*: " << res_star
       << "\n\n";
    locked_print(std::cout, os.str());
#endif
    state_ptr_ = &Mobile_entity::waiting_ack;
    return msg;
}

Msg_response Mobile_entity::waiting_ack(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << "ME: Waiting_ack state called for " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    if (bb[f_rc] != rc_auth_ack) {
        locked_print(
          std::cout, "ME: unexpected reason code waiting for auth_response\n");
        return std::nullopt;
    }
    amf_data_.authenticated_ok = true;
    amf_data_.uplink_nas_count_ = 0;// Just set to 0 for now !!!
    amf_data_.downlink_nas_count_ = 0;// Just set to 0 for now !!!
    amf_data_.supi_ = usim_.get_supi();
    amf_data_.imsi_ = amf_data_.supi_;// IMSI the same as SUPI?????
    amf_data_.ksi_ = bb[hdr_size + suci_size];
    amf_data_.abba_ = bb.get_part(hdr_size + suci_size + 1, abba_size);

    Byte_buffer kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::amf) };
    kdf_S += params_to_kdf_string(amf_data_.imsi_, amf_data_.abba_);
    amf_data_.k_amf_ = hmac_sha256(amf_data_.k_seaf_, kdf_S);

    Byte_buffer at{ static_cast<Byte>(Kdf_access_type::three_gpp) };
    kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::gNB) };
    kdf_S +=
      params_to_kdf_string(uint32_to_bb(amf_data_.uplink_nas_count_), at);

    k_gnb_ = hmac_sha256(amf_data_.k_amf_, kdf_S);
    current_next_hop_data_ = initialise_next_hop_data(k_gnb_);

#ifdef DEBUG
    os << "\n  ME: kdf_S: " << kdf_S << '\n';
    os << "  ME: K_GNB: " << k_gnb_ << "\n";
    os << "  ME:   NCC: " << current_next_hop_data_.ncc_ << "\n";
    os << "  ME:    NH: " << current_next_hop_data_.nh_ << "\n\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
#ifdef PRINT
    os << "ME: Authentication OK, calculated K_AMF and K_GNB\n\n";
    os << " K_AMF: " << amf_data_.k_amf_ << '\n';
    os << " K_GNB: " << k_gnb_ << "\n";
    os << "   NCC: " << current_next_hop_data_.ncc_ << "\n";
    os << "    NH: " << current_next_hop_data_.nh_ << "\n\n";
    locked_print(std::cout, os.str());
#endif
    state_ptr_ = &Mobile_entity::active;

    return std::nullopt;
}

Msg_response Mobile_entity::active(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << "ME: Active state called for " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    auto ref = static_cast<Ref_point>(bb[f_ref]);
    if (ref == Ref_point::rrc) {
        if (bb[f_rc] == rc_xn_ho_me) {
            Byte_buffer suci = bb.get_part(hdr_size, suci_size);
            gnb_id_star_ = bb[hdr_size + suci_size];
            crnti_star_ = bb.get_part(hdr_size + suci_size + 1, crnti_size);
            uint32_t ncc = bb_to_uint32(bb.get_part(
              hdr_size + suci_size + 1 + crnti_size, sizeof(uint32_t)));
            Byte_buffer kdf_S{ get_kdf_fc(Kdf_id::ng_ran_star_g) };
            Byte_buffer target_id{ gnb_id_star_ };
            kdf_S += params_to_kdf_string(
              target_id);// Just include the target ID number ??
#ifdef DEBUG
            os << "\n  kdf_S: " << kdf_S << "\n";
            os << "    NCC: " << ncc << "\n\n";
            locked_print(std::cout, os.str());
            os.str("");
#endif
            bool doing_vertical = (ncc > current_next_hop_data_.ncc_);
            if (doing_vertical) {
                update_next_hop_data(
                  amf_data_.k_amf_, current_next_hop_data_, ncc);
                k_gnb_star_ = hmac_sha256(current_next_hop_data_.nh_, kdf_S);
            } else {
                k_gnb_star_ = hmac_sha256(k_gnb_, kdf_S);
            }
#ifdef PRINT
            os << "\n" << m_to_g << "\n\ngNB_id*: " << 0 + gnb_id_star_ << '\n';
            os << "C-RNTI*: " << crnti_star_ << '\n';
            os << " K_GNB*: " << k_gnb_star_ << '\n';
            locked_print(std::cout, os.str());
#endif
            Byte_buffer msg{ id_, gnb_id_star_, Ref_point::rrc, rc_xn_ho_cfrm };
            msg += suci;
            crnti_ = crnti_star_;
            k_gnb_ = k_gnb_star_;
            crnti_star_.clear();
            k_gnb_star_.clear();
            gnb_id_ = gnb_id_star_;
            gnb_id_star_ = unset_ID;
            // in_handover_ = false;
#ifdef DEBUG
            os.str("");
            os << "\nME" << 0 + id_ << ": handover completed to gNB "
               << 0 + gnb_id_ << "\n\n";
            locked_print(std::cout, os.str());
#endif
            return msg;
        }
        if (bb[f_rc] == rc_n2_ho_cmd) {
            Byte_buffer suci = bb.get_part(hdr_size, suci_size);
            gnb_id_star_ = bb[hdr_size + suci_size];
            size_t offset = hdr_size + suci_size + 1;
            uint32_t ncc = bb_to_uint32(bb.get_part(offset, sizeof(uint32_t)));
            offset += sizeof(uint32_t);
            Nasc nasc = unpack_nasc(bb.get_part(offset, nasc_bb_size));
            if (nasc.amf_status_ == unchanged) {
                update_next_hop_data(
                  amf_data_.k_amf_, current_next_hop_data_, ncc);
                Byte_buffer kdf_S{ get_kdf_fc(Kdf_id::ng_ran_star_g) };
                Byte_buffer target_id{ gnb_id_star_ };
                kdf_S += params_to_kdf_string(
                  target_id);// Just include the target ID number ??
                k_gnb_star_ = hmac_sha256(current_next_hop_data_.nh_, kdf_S);
            } else if (nasc.amf_status_ == changed) {
                Byte_buffer kdf_S{ get_kdf_fc(Kdf_id::amf_prime),
                    static_cast<Byte>(Amf_direction::ho) };
                kdf_S += params_to_kdf_string(
                  uint32_to_bb(amf_data_.downlink_nas_count_));
                k_amf_prime_ = hmac_sha256(amf_data_.k_amf_, kdf_S);
#ifdef DEBUG
                os << " kdf_S: " << kdf_S << '\n';
                os << "K_AMF': " << k_amf_prime_ << '\n';
                locked_print(std::cout, os.str());
                os.str("");
#endif
                Byte_buffer at{ static_cast<Byte>(Kdf_access_type::three_gpp) };
                kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::gNB) };
                kdf_S +=
                  params_to_kdf_string(uint32_to_bb(rekey_nas_count), at);
                new_k_gnb_ = hmac_sha256(k_amf_prime_, kdf_S);
#ifdef DEBUG
                os << " kdf_S: " << kdf_S << '\n';
                os << "K_gNB': " << new_k_gnb_ << '\n';
                locked_print(std::cout, os.str());
                os.str("");
#endif
                new_nhd_ = initialise_next_hop_data(new_k_gnb_);
            } else {
                locked_print(std::cout, "Invalid AMF key change status\n");
                return std::nullopt;
            }

            gnb_id_ = gnb_id_star_;
#ifdef PRINT
            os << "\nME" << 0 + id_ << " received handover data for gNB "
               << 0 + gnb_id_star_ << '\n';
            os << "AMF key is " << (nasc.amf_status_ == unchanged ? "not " : "")
               << "re-keyed\n";
#endif
            if (nasc.amf_status_ == unchanged) {
                k_gnb_ = k_gnb_star_;
                k_gnb_star_.clear();
            } else {
                amf_data_.k_amf_ = k_amf_prime_;
                k_amf_prime_.clear();
                current_next_hop_data_ = new_nhd_;
                new_nhd_ = Next_hop_data{};
                k_gnb_ = new_k_gnb_;
                new_k_gnb_.clear();
            }
#ifdef PRINT
            os << "\n" << m_to_g << "\n\ngNB_id*: " << 0 + gnb_id_ << '\n';
            os << "\n   K_AMF: " << amf_data_.k_amf_ << "\n";
            os << "      NH: " << current_next_hop_data_.nh_ << "\n";
            os << "     NCC: " << current_next_hop_data_.ncc_ << "\n";
            os << "   K_GNB: " << k_gnb_ << "\n";
            locked_print(std::cout, os.str());
#endif
            Byte_buffer msg{ id_, gnb_id_, Ref_point::rrc, rc_n2_ho_cfrm };
            msg += suci;
#ifdef DEBUG
            os << "\nSending N2 handover completion message to gNB "
               << 0 + gnb_id_ << "\n";
            debug_message(os, msg);
            locked_print(std::cout, os.str());
            os.str("");
#endif
            return msg;
        }
    }

    return std::nullopt;
}
