/*******************************************************************************
 * File:        Entity_shared_data.cpp
 * Description: Utilities for data shared between entities
 *
 * Created:     Wednesday 26 August 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include "Byte_buffer.h"
#include "Entity_shared_data.h"

Byte_buffer pack_nasc(Nasc const &nasc)
{
    Byte cf = static_cast<Byte>(nasc.amf_status_);
    Byte_buffer bb{ nasc.ksi_, cf };
    bb += uint32_to_bb(nasc.downlink_nas_count_);

    return bb;
}

Nasc unpack_nasc(Byte_buffer const &nasc_bb)
{
    Nasc nasc;
    nasc.ksi_ = nasc_bb[0];
    nasc.amf_status_ = static_cast<Nasc_kamf_change_flag>(nasc_bb[1]);
    nasc.downlink_nas_count_ = bb_to_uint32(nasc_bb.get_part(2, 4));

    return nasc;
}
