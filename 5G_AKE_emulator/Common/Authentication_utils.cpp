/*******************************************************************************
 * File:        Authentication_utils.cpp
 * Description: Wrappng for the milenage functions used in AKA
 *
 * Created:     Wednesay 30 April 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/

#include <iostream>
#include <sstream>
#include "milenage_f1to5.h"
#include "Byte_buffer.h"
#include "Ecies.h"
#include "Ecies_aes.h"
#include "Key_derivation.h"
#include "Model_param.h"
#include "Authentication_utils.h"

void locked_print(std::ostream &os, std::string const &str);

void set_op_value(Byte_buffer const &op)
{
    // set OP value (which is used as a global variable in milenage_f1to5.cpp,
    // abstracted from 3GPP  35206-e00)
    for (size_t i = 0; i < op.size(); ++i) { OP[i] = op[i]; }
}

Byte_buffer get_auth_mac(
  Byte_buffer key, Byte_buffer rand, Byte_buffer sqn, Byte_buffer amf)
{
    // Input values copied in to pass to f1

    // OP value must be set (this is used as a global variable in
    // milenage_f1to5.cpp, abstracted from 3GPP  35206-e00)

    Byte_buffer mac(8, 0x00);// Must initialise buffer ready for passing to the
                             // C routine, so use this constructor
    f1(key.data(), rand.data(), sqn.data(), amf.data(), mac.data());

    return mac;
}

Auth_data get_auth_data(Byte_buffer key, Byte_buffer rand)
{
    // Input values copied in to pass to f2345

    // OP value must be set (this is used as a global variable in
    // milenage_f1to5.cpp, abstracted from 3GPP  35206-e00)

    Auth_data aud;
    Byte_buffer res(
      xres_size, 0x00);// Must initialise buffer ready for passing to the C
                       // routine, so use this constructor
    Byte_buffer ck(ck_size, 0x00);// Must initialise buffer ready for passing to
                                  // the C routine, so use this constructor
    Byte_buffer ik(ik_size, 0x00);// Must initialise buffer ready for passing to
                                  // the C routine, so use this constructor
    Byte_buffer ak(ak_size, 0x00);// Must initialise buffer ready for passing to
                                  // the C routine, so use this constructor

    f2345(key.data(), rand.data(), res.data(), ck.data(), ik.data(), ak.data());

    aud = std::make_pair(Usim::Auth_result{ res, ck, ik }, ak);

    return aud;
}

Usim::Ecies_data Usim::get_ecies_data() const
{
    auto res = ue_generate_eph_data(op_pub_key_);
    Byte_buffer eph_pub_bb_for_op = res.first;
    Byte_buffer ue_shared_secret = res.second;

    Byte_buffer ue_kdf_result = ecies_kdf(ue_shared_secret);

    Byte_buffer ue_aes_key = ue_kdf_result.get_part(0, AES_BLOCK_SIZE);
    Byte_buffer ue_hash_key = ue_kdf_result.get_part(AES_BLOCK_SIZE, 32);

    Byte_buffer initial_iv = initialise_iv(AES_BLOCK_SIZE);

    Byte_buffer network = usd_.supi.get_part(0, network_size);
    Byte_buffer msin = usd_.supi.get_part(network_size, msin_size);

    Byte_buffer suci =
      network + encrypt_id(msin, ue_aes_key, initial_iv).get_part(0, msin_size);
    // ecd[0] - suci, ecd[1] - ephemeral public key + iv, ecd[2] - hash key
    return Ecies_data{ suci, eph_pub_bb_for_op + initial_iv, ue_hash_key };
}

Usim::Auth_result Usim::authenticate(Byte_buffer const &auth_data_received)
{

    Byte_buffer amf_received =
      auth_data_received.get_part(rand_size + sqn_size, amf_size);
    // Check AMF here !!!!!!!!!!!!!!!!!!!!!!

    // set OP value (which is used as a global variable in milenage_f1to5.cpp,
    // abstracted from 3GPP  35206-e00)
    set_op_value(op_);

    Byte_buffer rand = auth_data_received.get_part(0, rand_size);

    Auth_data auth_data = get_auth_data(usd_.key, rand);

    Byte_buffer const &ak = auth_data.second;
    Byte_buffer c_sqn_received =
      auth_data_received.get_part(rand_size, sqn_size);
    Byte_buffer r_sqn(c_sqn_received);
    for (size_t i = 0; i < c_sqn_received.size(); ++i) {
        r_sqn[i] = c_sqn_received[i]
                   ^ ak[i];// Calcultate c_sqn_received xor ak
                           // Check SQN here!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

    Byte_buffer mac_a = get_auth_mac(usd_.key, rand, usd_.sqn, usd_.amf);

    Byte_buffer mac_received =
      auth_data_received.get_part(rand_size + sqn_size + amf_size, mac_size);
    // Check MAC here!!!!!!!!!!!!!!!!!

#ifdef DEBUG
    std::ostringstream os;
    os << "USIM: MAC: " << mac_a << '\n';
    locked_print(std::cout, os.str());
    os.str("");
    os << "USIM: SQN: " << r_sqn << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif

    return auth_data.first;
}
