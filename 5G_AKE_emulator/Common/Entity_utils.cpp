/*******************************************************************************
 * File:        Entity_utils.cpp
 * Description: Entity utilities for the 5G dmeonstrations
 *
 * Created:     Monday 15 November 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <thread>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <mutex>
#include "Model_param.h"
#include "MT_queue.h"
#include "Msg_handler.h"
#include "Entity.h"
#include "Msg_router.h"
#include "Entity_utils.h"


void setup_entity(Entity &e,
  Entity_msg_handler &mh,
  Entity_queues &eq,
  Mesg_router &mr,
  Entity_threads_map &tm)
{
    Id_type id = e.get_id();
    Msg_queue_ptr eq_ptr = eq.get_queue();
    Msg_queue_ptr r_ptr = mr.get_input_queue_ptr();

    e.assign_input_queue(eq_ptr);
    e.assign_router_queue(r_ptr);
    mr.register_entity_queue(id, eq_ptr);
    mh.assign_input_queue(eq_ptr);
    mh.assign_router_queue(r_ptr);
    auto res = tm.emplace(id, Entity_thread_data{ id, eq_ptr });
    if (!res.second) {
        throw std::runtime_error(
          "Failed to insert element into the threads map\n");
    }
}

void close_down_the_threads(Entity_threads_map &ethm)
{
    for (auto &th : ethm) {
        th.second.qptr_->push_message(
          Byte_buffer{ terminate_ID, th.second.id_, Ref_point::cmd });
        th.second.th_.join();
    }
}
