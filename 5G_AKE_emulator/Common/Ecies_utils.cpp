/*******************************************************************************
 * File:        Ecies_utils.cpp
 * Description: Utility functions for ecies
 *
 * Created:     Saturday 17 March 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/

#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/ec.h>
#include <openssl/pem.h>

#include "Openssl_utils.h"
#include "Ecies_utils.h"

Byte_buffer public_key_to_byte_buffer(EC_KEY *pkey)
{
    const EC_GROUP *ec_group = EC_KEY_get0_group(pkey);
    const EC_POINT *pub = EC_KEY_get0_public_key(pkey);
    BIGNUM *pub_bn = BN_new();
    BN_CTX *pub_bn_ctx = BN_CTX_new();

    // Get the key size
    size_t plen = EC_POINT_point2oct(
      ec_group, pub, POINT_CONVERSION_COMPRESSED, nullptr, 0, pub_bn_ctx);

    //	std::cout << "EC_POINT_point_to_oct returned plen: " << plen << '\n';
    Byte_buffer bb(plen, 0);
    // Get the key
    plen = EC_POINT_point2oct(ec_group,
      pub,
      POINT_CONVERSION_COMPRESSED,
      bb.data(),
      bb.size(),
      pub_bn_ctx);

    //	std::cout << "When converting, EC_POINT_point2oct returned plen: " <<
    // plen << '\n';

    //	std::cout << "Byte_buffer created, length " << bb.length() << "\n data:
    //" << bb.to_hex_string() << '\n';

    BN_CTX_free(pub_bn_ctx);
    BN_clear_free(pub_bn);
    return bb;
}

int public_key_byte_buffer_to_point(
  const EC_GROUP *ec_group, Byte_buffer const &bb, EC_POINT **pubk_point)
{
    // BIGNUM *pubk_bn=nullptr;
    BN_CTX *pub_bn_ctx;

    //	std::cout << "Converting a byte buffer to an EC_POINT. Byte buffer size
    //" << bb.length() << '\n';
    *pubk_point = EC_POINT_new(ec_group);

    pub_bn_ctx = BN_CTX_new();

    int res = EC_POINT_oct2point(
      ec_group, *pubk_point, bb.cdata(), bb.size(), pub_bn_ctx);
    if (res <= 0) {
        std::cerr << "Call to EC_POINT_oct2point failed\n";
        exit(0);
    }


    //	std::cout << "When converting, EC_POINT_oct2point returned res: " << res
    //<< '\n';

    BN_CTX_free(pub_bn_ctx);
    // BN_clear_free(pubk_bn);

    return 0;
}

EC_KEY *get_op_public_key_from_file(std::string const &basename)
{
    std::string filename = basename + "-pub-key.pem";

    BIO *b = BIO_new_file(filename.c_str(), "r");
    if (b == nullptr) {
        printf("Public key PEM file read failed\n");
        handle_openssl_error();
        BIO_free(b);
        return nullptr;
    }
    //	else
    //		printf("File %s loaded\n", filename.c_str());

    EVP_PKEY *pkey = nullptr;
    EC_KEY *op_pub_key = nullptr;// EC key from key data

    EVP_PKEY *res = PEM_read_bio_PUBKEY(b, &pkey, nullptr, nullptr);
    if (res == nullptr) {
        printf("PEM read failed\n");
        BIO_free(b);

        return nullptr;
    }
    //	else
    //		printf("Public key PEM file read successfully\n");

    op_pub_key = EVP_PKEY_get1_EC_KEY(pkey);
    if (res == nullptr) {
        printf("Converting to EC_KEY failed\n");
        BIO_free(b);
        return nullptr;
    }
    //	else
    //		printf("EC_KEY created successfully\n");

    free(pkey);
    BIO_free(b);

    return op_pub_key;
}


EC_KEY *get_op_private_key_from_file(std::string const &basename)
{
    std::string filename = basename + "-priv-key.pem";

    BIO *b = BIO_new_file(filename.c_str(), "r");
    if (b == nullptr) {
        printf("Private key PEM file - read failed\n");
        handle_openssl_error();
        BIO_free(b);
        return nullptr;
    }
    //	else
    //		printf("File %s loaded\n", filename.c_str());

    EVP_PKEY *pkey = nullptr;
    EC_KEY *op_priv_key = nullptr;// EC key from key data

    EVP_PKEY *res = PEM_read_bio_PrivateKey(b, &pkey, nullptr, nullptr);
    if (res == nullptr) {
        printf("PEM read failed\n");
        BIO_free(b);
        return nullptr;
    }
    //	else
    //		printf("Private key PEM file read successfully\n");

    op_priv_key = EVP_PKEY_get1_EC_KEY(pkey);
    if (res == nullptr) {
        printf("Converting to EC_KEY failed\n");
        BIO_free(b);
        free(pkey);
        return nullptr;
    }
    //	else
    //		printf("EC_KEY created successfully\n");

    BIO_free(b);
    free(pkey);

    return op_priv_key;
}
