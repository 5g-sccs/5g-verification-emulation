/*******************************************************************************
 * File:        Arpf_entity.cpp
 * Description: Code for the ARPF entity
 *
 * Created:     Thursday 21 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/

#include <sstream>
#include <iomanip>
#include <ctime>
#include "Byte_buffer.h"
#include "Hmac.h"
#include "Get_random_bytes.h"
#include "Msg_handler.h"
#include "Entity_shared_data.h"
#include "Key_derivation.h"
#include "Model_param.h"
#include "Authentication_utils.h"
#include "Arpf_entity.h"

void ARPF_entity::report(std::ostream &os) const
{
    os << "Report for ARPF: " << 0 + id_ << '\n';
    os << "ARPF has " << users_.size() << " subscribers\n";
    os << "ARPF is handling " << arpf_fsms_.size() << " UEs\n";
}

Msg_response ARPF_entity::process_message(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << "ARPF received: " << bb.to_hex_string() << '\n';
    locked_print(std::cout, os.str());
    os.str("");
    Id_type source = bb[f_srce];
#endif
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
#ifdef DEBUG
    os << "ARPF received suci: " << suci << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    auto const pos = arpf_fsms_.find(suci);
    if (pos != arpf_fsms_.end()) { return pos->second->update_state(bb); }
    Byte_buffer supi = get_ecies_data(bb);
#ifdef DEBUG
    os << "ARPF calculated supi: " << supi.to_hex_string() << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    auto const upos = users_.find(supi);
    ARPF_fsm *fsm_ptr{ nullptr };
    USIM_data ud;
    if (upos != users_.end()) {
        ud = upos->second;// Get users data
        fsm_ptr = new ARPF_fsm(id_, ausf_id_, suci, ud, this);
        if (fsm_ptr == nullptr) {
            throw(std::runtime_error("Unable to allocate memory for ARPF_fsm"));
        }
        arpf_fsms_.emplace(suci, fsm_ptr);
#ifdef DEBUG
        os << "ARPF: new state machine created for UE: " << source << '\n';
        locked_print(std::cout, os.str());
#endif
        return fsm_ptr->update_state(bb);
    }
    os.str("");
    os << "ARPF: no subscriber data for supi " << supi << '\n';
    locked_print(std::cout, os.str());
    os.str("");
    return std::nullopt;
}

ARPF_entity::~ARPF_entity()
{
    std::ostringstream os;
#ifdef DEBUG
    os << "ARPF " << 0 + id_ << " destructor. Handling " << arpf_fsms_.size()
       << " MEs\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    for (auto &f : arpf_fsms_) {
#ifdef DEBUG
        os << "Deleting ARPF_fsm for UE: " << f.first << '\n';
        locked_print(std::cout, os.str());
        os.str("");
#endif
        delete f.second;
    }
}

Byte_buffer ARPF_entity::get_ecies_data(Byte_buffer const &bb) const
{
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
    size_t offset = hdr_size + suci_size + sn_name_size;
    size_t eph_key_size = bb.size() - offset - iv_size - mac_tag_size;
    Byte_buffer eph_pub_key = bb.get_part(offset, eph_key_size);

    Byte_buffer op_shared_secret =
      op_generate_eph_data(ec_priv_key_, eph_pub_key);
    Byte_buffer op_kdf_result = ecies_kdf(op_shared_secret);
    offset += eph_key_size;
    Byte_buffer initial_iv = bb.get_part(offset, iv_size);
    offset += iv_size;
    Byte_buffer ue_mac = bb.get_part(offset, mac_tag_size);

    Byte_buffer op_aes_key = op_kdf_result.get_part(0, AES_BLOCK_SIZE);
    Byte_buffer op_hash_key = op_kdf_result.get_part(AES_BLOCK_SIZE, 32);

    Byte_buffer op_mac =
      hmac_sha256(op_hash_key, suci + eph_pub_key + initial_iv)
        .get_part(0, mac_size);
    if (!(ue_mac == op_mac)) {
        locked_print(std::cout, "MAC check failed in ARPF\n");
        exit(1);
    }

    Byte_buffer network = suci.get_part(0, network_size);
    Byte_buffer msin_c = suci.get_part(
      network_size, msin_size);//+Byte_buffer(AES_BLOCK_SIZE-msin_size,0);
    Byte_buffer msin =
      encrypt_id(msin_c, op_aes_key, initial_iv).get_part(0, msin_size);

    return network + msin;
}


void ARPF_entity::add_user(USIM_data const &ud)
{
    auto result = users_.emplace(ud.supi, ud);
    std::ostringstream os;
#ifdef DEBUG
    os << "Inserting new user into the subscribers map\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    if (!result.second) {
        os << "User already inserted, list unchanged\n";
        locked_print(std::cout, os.str());
    }
#ifdef DEBUG
    os.str("");
    os << "Map has " << users_.size() << " subscribers\n";
    locked_print(std::cout, os.str());
#endif
}

Byte_buffer ARPF_entity::generate_HE_AV(
  USIM_data const &usd, Byte_buffer const &sn_name) const
{
    std::ostringstream os;
    auto seed = seed_from_clock();
    Byte_buffer rand = get_random_bytes(rand_size, seed);

    Byte_buffer key = usd.key;
    Byte_buffer sqn = usd.sqn;
    Byte_buffer amf = usd.amf;

    // set OP value (which is used as a global variable in milenage_f1to5.cpp,
    // abstracted from 3GPP  35206-e00)
    set_op_value(op_);

    Byte_buffer mac_a = get_auth_mac(usd.key, rand, usd.sqn, usd.amf);
#ifdef DEBUG
    os << "ARPF: MAC: " << mac_a << '\n';
    locked_print(std::cout, os.str());
    os.str("");
    os << "ARPF: SQN: " << sqn << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Auth_data auth_data = get_auth_data(usd.key, rand);

    Byte_buffer const &res = auth_data.first[0];
    Byte_buffer const &ck = auth_data.first[1];
    Byte_buffer const &ik = auth_data.first[2];

    Byte_buffer const &ak = auth_data.second;
    Byte_buffer c_sqn(sqn);
    for (size_t i = 0; i < sqn.size(); ++i) {
        c_sqn[i] = sqn[i] ^ ak[i];// Calcultate sqn xor ak
    }

    Byte_buffer autn(c_sqn);
    autn += amf;
    autn += mac_a;
#ifdef DEBUG
    os << "ARPF: RES: " << res << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer av(rand);
    av += autn;

    Byte_buffer Lv{ 0, static_cast<Byte>(sn_name_size) };
    Byte_buffer kdf_S{ get_kdf_fc(Kdf_id::ausf) };
    kdf_S += params_to_kdf_string(sn_name, c_sqn);

    Byte_buffer kdf_key = ck + ik;
    Byte_buffer k_ausf = hmac_sha256(kdf_key, kdf_S);

    kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::xres_star) };
    Lv[1] = static_cast<Byte>(sn_name_size);
    kdf_S += sn_name + Lv;
    Lv[1] = rand_size;
    kdf_S += rand + Lv;
    Lv[1] = xres_size;
    kdf_S += res + Lv;
    Byte_buffer xres_star =
      hmac_sha256(kdf_key, kdf_S)
        .get_part(sha256_bytes - xres_star_size, xres_star_size);

    av += xres_star + k_ausf;
#ifdef DEBUG
    os << "ARPF: RAND: " << rand << '\n';
    locked_print(std::cout, os.str());
    os.str("");
    os << "ARPF: AUTN:" << autn << '\n';
    os << "ARPF: XRES*: " << xres_star << '\n';
    locked_print(std::cout, os.str());
    os.str("");
    os << "ARPF: K_AUSF: " << k_ausf << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    return av;
}

Msg_response ARPF_fsm::update_state(Byte_buffer const &bb)
{
    Msg_response response = ((*this).*(state_ptr_))(bb);
    if (response != std::nullopt) {
        arpf_ptr_->router_queue_ptr_->push_message(response.value());
    }

    return std::nullopt;// For now
}

Msg_response ARPF_fsm::new_request(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    os << "ARPF_fsm::new_request called for: " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
    if (bb[f_rc] != rc_auth_req) {
        locked_print(std::cout, "ARPF: unexpected reason code in state 1\n");
        Byte_buffer msg;
        return std::nullopt;
    }

    sn_name_ = bb.get_part(hdr_size + suci_size, sn_name_size);
    Byte_buffer he_av = arpf_ptr_->generate_HE_AV(usd_, sn_name_);
    Byte_buffer msg{ id_, ausf_id_, Ref_point::n13, rc_auth_resp };
    msg += suci;
    msg += usd_.supi;
    msg += he_av;
#ifdef PRINT
    locked_print(std::cout,
      la_to_a + '\n' + aa_space
        + "Nudm_UEAuthentication_Get Response\n\n[SUPI]: "
        + usd_.supi.to_hex_string() + "\n  RAND: "
        + he_av.get_part(0, rand_size).to_hex_string() + "\n  AUTN: "
        + he_av.get_part(rand_size, autn_size).to_hex_string() + "\n XRES*: "
        + he_av.get_part(rand_size + autn_size, get_kdf_size(Kdf_id::xres_star))
            .to_hex_string()
        + "\nK_AUSF: "
        + he_av
            .get_part(
              rand_size + autn_size + xres_size, get_kdf_size(Kdf_id::ausf))
            .to_hex_string()
        + "\n\n");
#endif
    state_ptr_ = &ARPF_fsm::active;
    return msg;
}

Msg_response ARPF_fsm::active(Byte_buffer const &bb)
{
    locked_print(
      std::cout, "ARPF_fsm:active called for " + bb.to_hex_string() + '\n');
    Byte_buffer msg;

    return msg;
}
