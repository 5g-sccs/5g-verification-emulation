/*******************************************************************************
 * File:        Msg_utils.cpp
 * Description: Message utilities
 *
 * Created:     Monday 30 August 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#include <iostream>
#include <sstream>
#include "Model_param.h"
#include "Byte_buffer.h"
#include "Msg_utils.h"

// Quick hack to get some data for the paper - make these global
Ho_msg_data_count ho_msg_count;
Ho_count_data ho_msg_counts;

void reset_ho_msg_count() { ho_msg_count = Ho_msg_data_count{}; }

Msg_timer ho_timer;
Msg_timings ho_timings;


void debug_message(std::ostream &os, Byte_buffer const &msg)
{
    os << std::hex;
    size_t msg_size = msg.size();
    if (msg_size < hdr_size + suci_size) {
        os << "Msg:" << msg << "\n\n";
    } else {
        os << " Header: " << 0 + msg[f_srce] << " " << 0 + msg[f_dest] << " "
           << 0 + msg[f_ref] << " " << 0 + msg[f_rc] << '\n';
        os << "   SUCI: " << msg.get_part(hdr_size, suci_size) << '\n';
        if (msg_size > hdr_size + suci_size) {
            os << "Payload: "
               << msg.get_part(
                    hdr_size + suci_size, msg.size() - hdr_size - suci_size)
               << "\n\n";
        } else {
            os << "No payload\n\n";
        }
    }
}

void print_message(std::ostream &os, Byte_buffer const &msg)
{
    size_t msg_size = msg.size();
    if (msg_size < hdr_size + suci_size) {
        os << "Msg:" << msg << "\n\n";
    } else {
        os << "| " << entity_str(msg[f_srce]) << " | "
           << entity_str(msg[f_dest]) << " | " << reference_point(msg[f_ref])
           << " | " << rc_str(msg[f_rc])
           << " | SUCI: " << msg.get_part(hdr_size, suci_size) << " | ";
        if (msg_size > hdr_size + suci_size) {
            os << "Payload: "
               << msg.get_part(
                    hdr_size + suci_size, msg.size() - hdr_size - suci_size)
               << "\n";
        } else {
            os << "No payload\n";
        }
    }
}

std::string entity_str(Id_type id)
{
    std::ostringstream os;
    if (id == 0) { return "ID unset"; }
    if (id < gNB_base_ID) {
        os << "  UE";
    } else if (id < amf_base_ID) {
        os << " gNB";
    } else if (id < ausf_base_ID) {
        os << " AMF";
    } else if (id < arpf_base_ID) {
        os << "AUSF";
    } else if (id < router_base_ID) {
        os << "ARPF";
    } else if (id < command_base_ID) {
        os << " RTR";
    } else {
        os << " CMD";
    }

    os << std::setw(2) << 0 + id;

    return os.str();
}

std::string reference_point(Byte rp)
{
    auto ref = static_cast<Ref_point>(rp);
    switch (ref) {
    case cmd:
        return "CMD";
    case rrc:
        return "RRC";
    case n1:
        return " N1";
    case n2:
        return " N2";
    case xn:
        return " Xn";
    case n12:
        return "N12";
    case n13:
        return "N13";
    }

    return "Invalid ref";
}

std::string rc_str(Byte rc)
{
    auto pos = rc_map.find(rc);
    if (pos == rc_map.end()) {
        std::ostringstream os;
        os << "unknown rc: " << 0 + rc;
        return os.str();
    }

    return pos->second;
}
