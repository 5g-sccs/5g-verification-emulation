/*******************************************************************************
 * File:        Ausf_entity.cpp
 * Description: Code for the AUSF entity
 *
 * Created:     Thursday 21 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/

#include <sstream>
#include <iomanip>
#include "Ausf_entity.h"
#include "Msg_handler.h"
#include "Key_derivation.h"
#include "Model_param.h"

void AUSF_entity::report(std::ostream &os) const
{
    os << "Report for AUSF: " << 0 + id_ << '\n';
    os << "AUSF is handling " << ausf_fsms_.size() << " UEs\n";
}

Msg_response AUSF_entity::process_message(Byte_buffer const &bb)
{
#ifdef DEBUG
    std::ostringstream os;
    os << "AUSF received: " << bb << '\n';
    locked_print(std::cout, os.str());
    Id_type source = bb[f_srce];
#endif
    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
#ifdef DEBUG
    os << "AUSF checking for SUCI: " << suci << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    auto const pos = ausf_fsms_.find(suci);
    if (pos != ausf_fsms_.end()) { return pos->second->update_state(bb); }
    auto *fsm_ptr = new AUSF_fsm(id_, amf_id_, arpf_id_, suci, this);
    if (fsm_ptr == nullptr) {
        throw(std::runtime_error("Unable to allocate memory for AUSF_fsm"));
    }
    ausf_fsms_.emplace(suci, fsm_ptr);
#ifdef DEBUG
    os << "New state machine created for UE: " << source << '\n';
    locked_print(std::cout, os.str());
#endif
    return fsm_ptr->update_state(bb);
}

AUSF_entity::~AUSF_entity()
{
    std::ostringstream os;
#ifdef DEBUG
    os << "AUSF " << 0 + id_ << " destructor. Handling " << ausf_fsms_.size()
       << " MEs\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    for (auto &f : ausf_fsms_) {
#ifdef DEBUG
        os << "Deleting AUSF_fsm for UE: " << f.first << '\n';
        locked_print(std::cout, os.str());
        os.str("");
#endif
        delete f.second;
    }
}


Msg_response AUSF_fsm::update_state(Byte_buffer const &bb)
{
    Msg_response response = ((*this).*(state_ptr_))(bb);
    if (response != std::nullopt) {
        ausf_ptr_->router_queue_ptr_->push_message(response.value());
    }

    return std::nullopt;// For now
}

Msg_response AUSF_fsm::new_request(Byte_buffer const &bb)
{
#ifdef DEBUG
    locked_print(std::cout,
      "AUSF_fsm:new_request called for " + bb.to_hex_string() + '\n');
#endif
    if (bb[f_rc] != rc_auth_req) {
        locked_print(
          std::cout, "AUSF: unexpected reason code in new_request\n");
        Byte_buffer msg;
        return std::nullopt;
    }
    Byte_buffer msg(bb);
    msg[f_srce] = id_;
    msg[f_dest] = arpf_id_;
    msg[f_ref] = Ref_point::n13;
    msg[f_rc] = rc_auth_req;

#ifdef PRINT
    size_t ecies_size = bb.size() - hdr_size - suci_size - sn_name_size;
    size_t key_length = ecies_size - iv_size - mac_tag_size;
    locked_print(std::cout,
      ra_to_a + "\n" + aa_space
        + "Nudm_UEAuthentication_Get Request\n\n  SUCI: "
        + suci_.to_hex_string() + "\n SN id: " + d_sn_name.to_hex_string()
        + "\nEP_KEY: "
        + bb.get_part(hdr_size + suci_size + sn_name_size, key_length)
            .to_hex_string()
        + "\n    IV: "
        + bb.get_part(hdr_size + suci_size + sn_name_size + key_length, iv_size)
            .to_hex_string()
        + "\n   MAC: "
        + bb.get_part(
              hdr_size + suci_size + sn_name_size + key_length + iv_size,
              mac_tag_size)
            .to_hex_string()
        + "\n\n");
#endif
    state_ptr_ = &AUSF_fsm::await_response;
    return msg;
}

Msg_response AUSF_fsm::await_response(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(std::cout,
      "AUSF_fsm::await_response called for " + bb.to_hex_string() + '\n');
#endif
    if (bb[f_rc] != rc_auth_resp) {
        locked_print(
          std::cout, "AUSF: unexpected reason code in await_response\n");
        return std::nullopt;
    }
    suci_ = bb.get_part(hdr_size, suci_size);
    supi_ = bb.get_part(hdr_size + suci_size, supi_size);
    Byte xres_star_offset =
      hdr_size + suci_size + supi_size + rand_size + autn_size;
    xres_star_ = bb.get_part(xres_star_offset, get_kdf_size(Kdf_id::xres_star));
#ifdef DEBUG
    os << "AUSF: XRES*: " << xres_star_ << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte k_ausf_offset = xres_star_offset + get_kdf_size(Kdf_id::xres_star);
    k_ausf_ = bb.get_part(k_ausf_offset, get_kdf_size(Kdf_id::ausf));
    Byte_buffer kdf_S = Byte_buffer{ get_kdf_fc(Kdf_id::seaf) };
    kdf_S += params_to_kdf_string(d_sn_name);
    k_seaf_ = hmac_sha256(k_ausf_, kdf_S);

    kdf_S = bb.get_part(hdr_size + suci_size + supi_size, rand_size);// RAND
    kdf_S += xres_star_;
    Byte_buffer hxres_star = sha256_bb(kdf_S).get_part(16, 16);
#ifdef DEBUG
    os << "AUSF: HXRES*: " << hxres_star << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    Byte_buffer msg(bb);
    msg[f_srce] = id_;
    msg[f_dest] = amf_id_;
    msg[f_ref] = Ref_point::n12;
    msg.set_part(xres_star_offset, hxres_star);

    msg.set_part(
      k_ausf_offset, k_seaf_);// Could just truncate here, but following 33.501
#ifdef PRINT
    os << a_to_s << '\n'
       << as_space << "Nausf_UEAuthentication_Authenticate Response\n\n[SUPI]:";
    os << supi_ << "\n  RAND: "
       << bb.get_part(hdr_size + suci_size + supi_size, rand_size);
    os << "\n  AUTN: "
       << bb.get_part(hdr_size + suci_size + supi_size + rand_size, autn_size);
    os << "\nHXRES*: " << hxres_star << "\n\n";
    locked_print(std::cout, os.str());
#endif
    msg.resize(k_ausf_offset);// Drops k_seaf, don't send this yet
    state_ptr_ = &AUSF_fsm::await_acknowledgement;
    return msg;
}

Msg_response AUSF_fsm::await_acknowledgement(Byte_buffer const &bb)
{
    std::ostringstream os;
#ifdef DEBUG
    locked_print(std::cout,
      "AUSF_fsm::await_acknowledgement called for " + bb.to_hex_string()
        + '\n');
#endif
    Byte_buffer msg;

    if (bb[f_rc] != rc_amf_auth_resp) {
        locked_print(
          std::cout, "AUSF: unexpected reason code in await_acknowledgement\n");
        return std::nullopt;
    }

    Byte_buffer suci = bb.get_part(hdr_size, suci_size);
    Byte_buffer res_star =
      bb.get_part(hdr_size + suci_size, get_kdf_size(Kdf_id::xres_star));

    if (res_star == xres_star_) {
        msg = Byte_buffer{ id_, amf_id_, Ref_point::n12, rc_ausf_auth_ack };
        msg += suci;
        msg += supi_;
        msg += k_seaf_;
#ifdef PRINT
        os << a_to_s << "\n"
           << as_space
           << "Nausf_UEAuthentication_Authenticate Response\n\n[SUPI]: ";
        os << supi_ << "\nK_SEAF: " << k_seaf_ << "\n\n";
        locked_print(std::cout, os.str());
#endif
        state_ptr_ = &AUSF_fsm::active;
        return msg;
    }

    locked_print(std::cout, "AUSF: Authentication not confirmed\n");

    return std::nullopt;
}

Msg_response AUSF_fsm::active(Byte_buffer const &bb)
{
#ifdef PRINT
    locked_print(
      std::cout, "AUSF_fsm::active called for " + bb.to_hex_string() + '\n');
#else
    (void)bb;
#endif
    Byte_buffer msg;

    return msg;
}
