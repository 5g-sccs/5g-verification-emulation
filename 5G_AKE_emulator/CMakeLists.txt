# Based on:
#
# https://github.com/lefticus/cpp_starter_project/blob/master/CMakeLists.txt
#

cmake_minimum_required(VERSION 3.13)

# Set the project name to your project name
project(5g_experiments CXX)

include(Cmake-options/StandardProjectSettings.cmake)

# Link this 'library' to set the c++ standard / compile-time options requested
add_library(project_options INTERFACE)
target_compile_features(project_options INTERFACE cxx_std_17)

# Link this 'library' to use the warnings specified in CompilerWarnings.cmake
add_library(project_warnings INTERFACE)

# standard compiler warnings
include(Cmake-options/CompilerWarnings.cmake)
set_project_warnings(project_warnings)

# sanitizer options if supported by compiler
include(Cmake-options/Sanitizers.cmake)
enable_sanitizers(project_options)

# enable doxygen
include(Cmake-options/Doxygen.cmake)
enable_doxygen()

# allow for static analysis options
include(Cmake-options/StaticAnalysers.cmake)

# Debugging option to print varaibles
#    e.g. cmake_print_variables(<variable>)
include(CMakePrintHelpers)

option(BUILD_SHARED_LIBS "Enable compilation of shared libraries" OFF)

set(utils_dir ${CMAKE_SOURCE_DIR}/Utilities)
set(LIBRARY_OUTPUT_PATH  ${CMAKE_SOURCE_DIR}/lib)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR}/bin) 
set(include_dirs ${utils_dir}/Include ${CMAKE_SOURCE_DIR}/Include)

set(Sources
    ${CMAKE_SOURCE_DIR}/Common/Arpf_entity.cpp
    ${CMAKE_SOURCE_DIR}/Common/Ausf_entity.cpp
    ${CMAKE_SOURCE_DIR}/Common/Ecies_aes.cpp
    ${CMAKE_SOURCE_DIR}/Common/Ecies.cpp
    ${CMAKE_SOURCE_DIR}/Common/Ecies_utils.cpp
    ${CMAKE_SOURCE_DIR}/Common/gNB_entity.cpp
    ${CMAKE_SOURCE_DIR}/Common/Key_derivation.cpp
    ${CMAKE_SOURCE_DIR}/Common/milenage_f1to5.cpp
    ${CMAKE_SOURCE_DIR}/Common/Mobile_entity.cpp
    ${CMAKE_SOURCE_DIR}/Common/Amf_entity.cpp
    ${CMAKE_SOURCE_DIR}/Common/MT_queue.cpp
    ${CMAKE_SOURCE_DIR}/Common/Authentication_utils.cpp
    ${CMAKE_SOURCE_DIR}/Common/Entity_shared_data.cpp
    ${CMAKE_SOURCE_DIR}/Common/Msg_utils.cpp
    ${CMAKE_SOURCE_DIR}/Common/Entity_utils.cpp
    ${utils_dir}/Common/Byte_buffer.cpp
    ${utils_dir}/Common/Clock_utils.cpp
    ${utils_dir}/Common/Hex_string.cpp
    ${utils_dir}/Common/Io_utils.cpp
    ${utils_dir}/Common/Openssl_utils.cpp
    ${utils_dir}/Common/Sha256.cpp
    ${utils_dir}/Common/Hmac.cpp
    ${utils_dir}/Common/Get_random_bytes.cpp
)


add_subdirectory(5g_handover_test_xn)
add_subdirectory(5g_handover_test_n2)
