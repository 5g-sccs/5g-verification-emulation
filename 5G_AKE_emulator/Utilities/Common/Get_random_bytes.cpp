/*******************************************************************************
* File:        Get_Random_byte_generator.cpp
* Description: Get a set of pseudo-random bytes
*
*
* Created:     Wednesay 30 May 2018
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/

#include <chrono>
#include <random>
#include "Get_random_bytes.h"

Random_byte_generator::seed_type seed_from_clock()
{
    return static_cast<Random_byte_generator::seed_type>(std::chrono::system_clock::now().time_since_epoch().count());
}

Random_byte_generator::Random_byte_generator(seed_type seed)
{
    auto s = (seed != 0) ? seed : seed_from_clock();
    dre.seed(s);
    rb = std::uniform_int_distribution<uint8_t>(0, 0xff);
    auto tmp = static_cast<Byte>(rb(dre));// Discard the first value ??
      // Yes if seeding from time(NULL), see:
      // https://stackoverflow.com/questions/26475595
      // Discard here to be on the safe side
    (void)tmp;// Stop unused variable warning
}

Byte_buffer Random_byte_generator::operator()(size_t number_of_bytes)
{
    Byte_buffer r_bytes(number_of_bytes, 0);
    for (size_t i = 0; i < number_of_bytes; ++i) {
        r_bytes[i] = static_cast<Byte>(rb(dre));
    }

    return r_bytes;
}

// Previous function, updated to use Random_byte_generator
Byte_buffer get_random_bytes(
  size_t number_of_bytes,
  Random_byte_generator::seed_type seed)
{
    Random_byte_generator rbg(seed);

    Byte_buffer result(number_of_bytes, 0);

    return rbg(number_of_bytes);
}
