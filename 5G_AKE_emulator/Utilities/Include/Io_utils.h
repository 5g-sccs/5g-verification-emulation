/*******************************************************************************
* File:        Io_utils.h
* Description: I/O utilities
*
* Created:     Wednesday 1 May 2013
*
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <iostream>
#include <sstream>
#include <string>

// Define the appropriate directory seperators and their opposites
#ifndef _WIN32
//#define _MAX_FNAME 1024
constexpr char dirSep = '/';
constexpr char altDirSep = '\\';
#else
constexpr char dirSep = '\\';
constexpr char altDirSep = '/';
#endif

// Terminal colours
auto constexpr blue = "\33[34m";
auto constexpr red = "\33[31m";
auto constexpr green = "\33[32m";
auto constexpr magenta = "\33[35m";
auto constexpr normal = "\33[0m";

constexpr int maxline = 200;

std::string make_filename(
  std::string const &baseDir,
  std::string const &name);

std::string get_environment_variable(
  std::string const &var,
  std::string def) noexcept;

void eat_white(std::istream &is);

std::string str_tolower(
  std::string const &str);

std::string str_toupper(
  std::string const &str);

void print_hex_byte(
  std::ostream &os,
  uint8_t byte);

void print_buffer(
  std::ostream &os,
  const uint8_t *buf,
  size_t len);

void print_buffer_as_chars(
  std::ostream &os,
  const uint8_t *buf,
  size_t len,
  uint8_t non_char_replacement);


// Recursive version of vars_to_string, may cause code to grow unnecessarily
// see https://www.youtube.com/watch?v=CU3VYN6xGzM, C++ Weekly for an
// alternative using initialiser lists
/* template<typename T>
std::string vars_to_string(T const &t)
{
    std::ostringstream os;
    os << t;
    return os.str();
}

template<typename T, typename... Vargs>
std::string vars_to_string(T t, Vargs... args)
{
    std::ostringstream os;
    os << t;
    return os.str() + vars_to_string(args...);
}
*/
// Alternative initialiser list version
// (A,B) - A is carried out first, then B. The result from B is returned
// (os << t, 0) - writes t to the stream and returns 0 to the <int> initializer list
// ... the parameter pack is expanded
template<typename... T>
std::string vars_to_string(const T &... t)
{
    std::ostringstream os;
    (void)std::initializer_list<int>{ (os << t, 0)... };
    return os.str();
}
