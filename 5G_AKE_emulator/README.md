# 5g_experiments

5G experiments using message passing between threads running on a single machine.

## Building the code
To build the code you will need cmake (version 3.13 or later) installed and a
C++17 compiler (gcc version 7, clang version 5, or later).

The code also needs OpenSSL (version 1.1, not version 3). cmake will try to find
OpenSSL, but should this fail then set the following environment variables:
LDFLAGS, CPPFLAGS and PKG_CONFIG_PATH, for example,

    export LDFLAGS="-L/usr/local/opt/openssl@3/lib"
    export CPPFLAGS="-I/usr/local/opt/openssl@3/include"
    export PKG_CONFIG_PATH="/usr/local/opt/openssl@3/lib/pkgconfig"

To build the code starting in the 5G_AKE_C2 directory:

    cd build
    cmake -S .. -B .
    make

Compiler options are set in Cmake-options/CompilerWarnings.cmake. Not all options
are available on all compiler versions; any options that are not recognised by
your compiler can be commented out in CompilerWarnings.cmake (using  #).

The executables will be in the build/bin directory.

Note that these build instructions were tried, for now, only on Ubuntu and on Mac OS, several distributions (e.g., Ubuntu 18.04, 20.04, Mac OS Catalina 10.15). Small changes (e.g., paths to Open SSL, etc.) may be needed for other OSs.

## Running the examples
First note that the PEM files must be available to the executables, so with the
PEM files in the build directory run (for example, from within the build folder):

    ./bin/5g_handover_test_xn T

or

    ./bin/5g_handover_test_n2 F.

5g_handover_test_xn runs two handovers, the options are:

    T - make the second handover a horizontal one, 
    F - do two vertical handovers.

5g_handover_test_n2 runs two handovers, the options are:

    T - use AMF re-keying when doing the handovers,
    F - no AMF re-keying when doing the handovers.

When running a program you will be prompted to enter h\<enter\> for a handover,
 or s\<enter\> to stop the program. 

## Controlling the output
The output produced by the programs is controlled by flags in the file
Model_param.h. They are:

    DEBUG - print extra information for debugging
    PRINT - print the protocol messages as it proceeds.
    MSG_LOG - print the actual messages sent to-and-fro
    MSG_COUNT - count the data transferred when doing handovers
    MSG_TIMING - Time the handovers, this option disables all of the others

When doing the message timing you just enter h\<enter\>, h\<enter\> and s\<enter\>
and the timings are returned.


