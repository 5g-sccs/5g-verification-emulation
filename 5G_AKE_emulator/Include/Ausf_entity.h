/*******************************************************************************
 * File:        AUSF_entity.h
 * Description: Definition of the AUSF class
 *
 * Created:     Thursday 21 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include "Byte_buffer.h"
#include "Hmac.h"
#include "Sha.h"
#include "MT_queue.h"
#include "Msg_handler.h"
#include "Entity.h"

void locked_print(std::string const &str);

class AUSF_fsm;

class AUSF_entity : public Entity
{
  public:
    friend AUSF_fsm;
    using Ausf_fsm_map = std::map<Byte_buffer, AUSF_fsm *>;
    AUSF_entity(Id_type id) : Entity(id) {}
    Id_type get_id() const { return id_; }
    void assign_amf_id(Id_type am_id) { amf_id_ = am_id; }
    void assign_arpf_id(Id_type ar_id) { arpf_id_ = ar_id; }

    Msg_response process_message(Byte_buffer const &bb) override;
    void report(std::ostream &os) const override;
    ~AUSF_entity() override;

  private:
    Id_type arpf_id_{ unset_ID };
    Id_type amf_id_{ unset_ID };

    Ausf_fsm_map ausf_fsms_;
};

class AUSF_fsm
{
  public:
    using AUSF_fn = Msg_response (AUSF_fsm::*)(Byte_buffer const &str);
    AUSF_fsm() = delete;
    AUSF_fsm(AUSF_fsm const &) = delete;
    AUSF_fsm(Id_type id,
      Id_type am_id,
      Id_type ar_id,
      Byte_buffer const &suci,
      AUSF_entity *ausf_ptr)
      : id_(id), amf_id_(am_id), arpf_id_(ar_id), suci_(suci),
        ausf_ptr_(ausf_ptr)
    {
        state_ptr_ = &AUSF_fsm::new_request;
    };
    Msg_response update_state(Byte_buffer const &bb);

  private:
    Id_type id_{ unset_ID };
    Id_type amf_id_{ unset_ID };
    Id_type arpf_id_{ unset_ID };
    Byte_buffer supi_;
    Byte_buffer suci_;
    Byte_buffer sn_id_;
    Byte_buffer k_ausf_;
    Byte_buffer k_seaf_;
    Byte_buffer xres_star_;

    AUSF_fn state_ptr_;
    AUSF_entity *ausf_ptr_{ nullptr };

    Msg_response new_request(Byte_buffer const &bb);
    Msg_response await_response(Byte_buffer const &bb);
    Msg_response await_acknowledgement(Byte_buffer const &bb);
    Msg_response active(Byte_buffer const &bb);
};
