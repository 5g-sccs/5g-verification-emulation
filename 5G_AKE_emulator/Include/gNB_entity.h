/*******************************************************************************
 * File:        gNB_entity.h
 * Description: The entity representing gNodeB (the radio)
 *
 * Created:     Tuesday 20 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <iostream>
#include <string>
#include "MT_queue.h"
#include "Entity.h"
#include "Entity_shared_data.h"

Byte_buffer generate_new_crnti();

struct Gnb_hop_data
{
    bool unused_{ false };
    Next_hop_data nhd_;
};

class gNB_fsm;

class gNB_entity : public Entity
{
  public:
    using Gnb_fsm_map = std::map<Byte_buffer, gNB_fsm *>;
    gNB_entity(Id_type id) : Entity(id) {}
    void assign_amf_id(Id_type id) { amf_id_ = id; }

    Msg_response process_message(Byte_buffer const &bb) override;
    void report(std::ostream &os) const override;
    ~gNB_entity() override;

  private:
    Id_type amf_id_{ unset_ID };
    Gnb_fsm_map gnb_fsms_;
};

class gNB_fsm
{
  public:
    using gNB_fn = Msg_response (gNB_fsm::*)(Byte_buffer const &str);
    // typedef Msg_response (gNB_fsm::*gNB_fn)(Byte_buffer const &str);
    gNB_fsm() = delete;
    gNB_fsm(gNB_fsm const &) = delete;
    gNB_fsm(Id_type gnb_id,
      Id_type me_id,
      Id_type amf_id,
      Byte_buffer const &suci,
      Byte_buffer const &crnti,
      Msg_queue_ptr rq_ptr);
    Msg_response update_state(Byte_buffer const &bb);

  private:
    Id_type gnb_id_{ unset_ID };
    Id_type me_id_{ unset_ID };
    Id_type amf_id_{ unset_ID };
    Byte_buffer suci_;
    Byte_buffer crnti_;
    Byte_buffer k_gnb_;
    bool doing_vertical_{ false };
    bool in_handover{ false };
    Gnb_hop_data current_next_hop_data_;
    Id_type gnb_ho_source_id_{ unset_ID };
    Id_type gnb_id_star_{ unset_ID };
    Byte_buffer crnti_star_;
    Byte_buffer k_gnb_star_;

    gNB_fn state_ptr_;
    Msg_queue_ptr router_queue_ptr_{ nullptr };

    Msg_response active(Byte_buffer const &bb);
    Msg_response handle_n2_message(Byte_buffer const &bb);
    Msg_response handle_rrc_message(Byte_buffer const &bb);
    Msg_response handle_cmd_message(Byte_buffer const &bb);
    Msg_response handle_xn_message(Byte_buffer const &bb);
    Msg_response trigger_xn_handover(Byte_buffer const &bb);
    Msg_response trigger_n2_handover(Byte_buffer const &bb);
    void assign_new_hop_data(Next_hop_data const &nhd);
};
