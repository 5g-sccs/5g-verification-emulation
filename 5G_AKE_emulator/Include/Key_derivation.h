/*******************************************************************************
 * File:        Key_derivation.h
 * Description: The key derivation functions
 *
 * Created:     Saturday 10 March 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

//#include <openssl/sha.h>

#include "Byte_buffer.h"
#include "Model_param.h"
#include "Entity_shared_data.h"

// Values used for key derivation
enum class Kdf_id {
    ausf,
    ck_prime,
    ik_prime,
    res_star,
    xres_star,
    hres_star,
    hxres_star,
    seaf,
    amf,
    nas_int,
    nas_enc,
    nh,
    ng_ran_star_g,
    ng_ran_star_e,
    gNB,
    n3iwf,
    rrc_int,
    rrc_enc,
    up_int,
    up_enc,
    amf_prime
};

struct Kdf_id_data
{
    Kdf_id id;
    Byte fc_value;
    Byte size;
};

// 33.501 Annex A - for FC values
constexpr Kdf_id_data kdf_data_table[] = {
    { Kdf_id::ausf, 0x6a, 32 },
    { Kdf_id::ck_prime, 0x6a, 32 },
    { Kdf_id::ik_prime, 0x6a, 32 },
    { Kdf_id::res_star, 0x6b, 16 },
    { Kdf_id::xres_star, 0x6b, 16 },
    { Kdf_id::hres_star, 0x00, 16 },// fc not used here, set to zero
    { Kdf_id::hxres_star, 0x00, 16 },// fc not used here, set to zero
    { Kdf_id::seaf, 0x6c, 32 },
    { Kdf_id::amf, 0x6d, 32 },
    { Kdf_id::nas_int, 0x69, 16 },
    { Kdf_id::nas_enc, 0x69, 16 },
    { Kdf_id::rrc_int, 0x69, 16 },
    { Kdf_id::rrc_enc, 0x69, 16 },
    { Kdf_id::up_int, 0x69, 16 },
    { Kdf_id::up_enc, 0x69, 16 },
    { Kdf_id::gNB, 0x6e, 32 },
    { Kdf_id::n3iwf, 0x6e, 32 },
    { Kdf_id::nh, 0x6f, 32 },
    { Kdf_id::ng_ran_star_g, 0x70, 32 },
    { Kdf_id::ng_ran_star_e, 0x71, 32 },
    { Kdf_id::amf_prime, 0x72, 32 },

};

// See 3GPP 33.501, Annex A.13
enum class Amf_direction {
    imm = 0x00,// Idle mode mobility
    ho = 0x01// Handover
};

enum class Kdf_access_type { three_gpp = 1, non_three_gpp };

constexpr uint32_t rekey_nas_count =
  0xffffffff;// Uplink NAS COUNT to be used when AMF re-keying during handover

enum class Kdf_alg_id {
    nas_enc = 1,// 33.501 Table A.8-1
    nas_int,
    rrc_enc,
    rrc_int,
    up_enc,
    up_int
};

struct Kdf_data
{
    Byte fc_value;
    Byte size;
};

inline constexpr Kdf_data get_kdf_data(Kdf_id const &id)
{
    Kdf_data res{};
    for (auto const &entry : kdf_data_table) {
        if (entry.id == id) {
            res = Kdf_data{ entry.fc_value, entry.size };
            break;
        }
    }
    return res;
}

inline constexpr Byte get_kdf_fc(Kdf_id const &id)
{
    Kdf_data kd = get_kdf_data(id);

    return kd.fc_value;
}

inline constexpr Byte get_kdf_size(Kdf_id const &id)
{
    Kdf_data kd = get_kdf_data(id);

    return kd.size;
}

// The parameters should be Byte_buffers - see how to check this
template<typename... T> Byte_buffer params_to_kdf_string(const T &... t)
{
    Byte_buffer res{};
    (void)std::initializer_list<int>{ (
      res += t + uint16_to_bb(static_cast<uint16_t>(t.size())), 0)... };
    return res;
}

Next_hop_data initialise_next_hop_data(Byte_buffer const &k_gnb);

void update_next_hop_data(
  Byte_buffer const &k_amf, Next_hop_data &nhd, uint32_t new_ncc);

Byte_buffer pack_next_hop_data(Next_hop_data const &nhd);

Next_hop_data unpack_next_hop_data(Byte_buffer const &nhd_bb);

constexpr size_t nhd_bb_size = sizeof(uint32_t) + get_kdf_size(Kdf_id::nh);

Byte_buffer ecies_kdf(Byte_buffer const &shared_secret);
