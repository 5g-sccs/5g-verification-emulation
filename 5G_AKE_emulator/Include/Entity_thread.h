/*******************************************************************************
 * File:        Entity_thread.h
 * Description: Entity threads for the 5G dmeonstrations
 *
 * Created:     Tuesday 2 June 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <thread>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <mutex>
#include "Model_param.h"
#include "MT_queue.h"
#include "Msg_handler.h"

// Data for each entity, id, queue being processed and thread
class Entity_thread_data
{
  public:
    using Queue_ptr = MT_queue<Byte_buffer>::Queue_ptr;
    Entity_thread_data() : id_(0), qptr_(nullptr) {}
    Entity_thread_data(Id_type id, Queue_ptr qp) : id_(id), qptr_(qp) {}

    Id_type id_;
    Queue_ptr qptr_;
    std::thread th_;// a default thread is created and it is assign to later
};

using Entity_threads_map = std::map<Id_type, Entity_thread_data>;
