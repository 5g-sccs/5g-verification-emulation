/*******************************************************************************
 * File:        Authentication_utils.h
 * Description: Wrappng for the milenage functions used in AKA
 *
 * Created:     Wednesay 30 April 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once
#include <openssl/ec.h>

#include "Byte_buffer.h"
#include "Model_param.h"

class Usim
{
  public:
    using Auth_result = std::array<Byte_buffer, 3>;// RES, CK and IK
    using Ecies_data =
      std::array<Byte_buffer, 3>;// suci, eph_pub_key+IV, hashing key
    Usim() {}
    Usim(USIM_data usd, Byte_buffer opb, EC_KEY *opk)
      : usd_(usd), op_(opb), op_pub_key_(opk)
    {}// and ECC public key !!!!!!
    Ecies_data get_ecies_data() const;
    Auth_result authenticate(Byte_buffer const
        &auth_data);// Checks input values and returns RES, CK and IK
    Byte_buffer get_supi() const { return usd_.supi; }

  private:
    USIM_data usd_;
    Byte_buffer op_;
    EC_KEY *op_pub_key_;
};

void set_op_value(Byte_buffer const &op);

Byte_buffer get_auth_mac(
  Byte_buffer key, Byte_buffer rand, Byte_buffer sqn, Byte_buffer amf);

using Auth_data = std::pair<Usim::Auth_result, Byte_buffer>;

Auth_data get_auth_data(Byte_buffer key, Byte_buffer rand);
