/*******************************************************************************
 * File:        Entity_shared_data.h
 * Description: Data shared between entities (once registered)
 *
 * Created:     Wednesday 10 June 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/

#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Byte_buffer.h"

/*
 * General structures used for several entities
 * /

/*
 * Data for the current next hop parameter
 */
constexpr uint32_t ncc_unset = 0;// 0xffffffff;// Good enough for our tests

struct Next_hop_data
{
    uint32_t ncc_{ ncc_unset };
    Byte_buffer nh_{};
};


/*
 * Data stored in structs and so generally available without accessors, possibly
 * change this and use friend classes once it is working and the requirements
 * are clear
 */

/*
 * The ARPF and UDM are treated as one for the moment and all of the traffic for
 * this entity comes via the AUSF. UDM data for a UE is stored tied to the SUCI.
 */

/*
 * The AMF and SEAF are treated as one for the moment. All of the traffic for
 * this entity comes from the AMF, or the ARPF.
 */
struct amf_ausf_shared_data// Stored on the AUSF tied to the AMF ID
{
    Byte_buffer k_ausf_{};
    // There may be more
};

/*
 * The AMF and SEAF are treated as one. All traffic comes via one of the gNBs,
 * or from the AUSF/UDM. As in reality, there is no direct UE - AMF link
 */

constexpr Byte ksi_unset{ 7 };// 3GPP 24.501 9.11.3.32, ngKSI = KSI as we are
                              // using IEI=0 and bit 4=0 for 3GPP access

// Check who derives the 5G-GUTI and when
struct Amf_ue_common_data
{
    bool authenticated_ok{ false };
    Byte_buffer abba_{};// Fixed in AMF and sent on.
    Byte_buffer supi_{};// Set once UE authenticates, sent from AUSF
    Byte_buffer imsi_{};
    Byte_buffer k_seaf_{};// Set once UE authenticates, sent from AUSF
    Byte ksi_{ ksi_unset };// Start with it unset. Set by AMF and then sent on.
    Byte_buffer k_amf_{};
    Byte_buffer k_nas_enc_{};
    Byte_buffer k_nas_int_{};
    uint32_t downlink_nas_count_{ 0 };// Size is 4 bytes, see 33.501 A13
    uint32_t uplink_nas_count_{ 0 };// Size is 4 bytes, see 33.501 A9
};

struct Amf_ue_data// Stored on AMF linked to SUCI and 5G-GUTI
{
    Amf_ue_common_data amf_ue_common_{};
    Next_hop_data current_nhd_{};
};

// NASC K_AMF change flags
enum Nasc_kamf_change_flag : Byte {
    unchanged = 1,
    changed = 2,
    flag_unset = 4
};

struct Nasc
{
    Byte ksi_{ ksi_unset };
    Nasc_kamf_change_flag amf_status_{ flag_unset };
    uint32_t downlink_nas_count_{ 0 };
    // NAS algorithms - not detailed here, we assume that they are unchanged
};

constexpr size_t nasc_bb_size = 6;

Byte_buffer pack_nasc(Nasc const &nasc);

Nasc unpack_nasc(Byte_buffer const &nasc_bb);

/* Possible useful structures - not used  (yet?)

struct gnb_amf_shared_data// Stored on AMF linked to RAN-ID
{
    uint32_t ncc_;// 33.501 A.9 length given as 4
    Byte_buffer nh_;
    Byte_buffer k_gnb_;
};

// Keys used for UE - gNB communication
struct Ue_gnb_communication_keys
{
    Byte_buffer rrc_enc_;
    Byte_buffer rrc_int_;
    Byte_buffer up_enc_;
    Byte_buffer up_int_;
};

struct Ue_gnb_shared_data// Stored on gNB tied to C-RNTI
{
    Byte_buffer crnti_;// Converted from a uint16_t
    uint16_t ncc_;// 33.501 A.9 length given as 4
    Byte_buffer nh_;
    Byte_buffer k_gnb_;
    Ue_gnb_communication_keys comms_keys_;
};
*/
