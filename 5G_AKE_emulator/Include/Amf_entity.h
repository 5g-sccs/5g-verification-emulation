/*******************************************************************************
 * File:        AMF_entity.h
 * Description: Definitiuon of the AMF class
 *
 * Created:     Thursday 21 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include "Byte_buffer.h"
#include "MT_queue.h"
#include "Msg_handler.h"
#include "Entity.h"

void locked_print(std::string const &str);

class AMF_fsm;

class AMF_entity : public Entity
{
  public:
    using Amf_fsm_map = std::map<Byte_buffer, AMF_fsm *>;
    AMF_entity(Id_type id) : Entity(id) {}
    void assign_ausf_id(Id_type ausf_id) { ausf_id_ = ausf_id; }
    void assign_gnb_id(Id_type gnb_id) { gnb_id_ = gnb_id; }

    Msg_response process_message(Byte_buffer const &bb) override;
    void report(std::ostream &os) const override;
    ~AMF_entity() override;

  private:
    Id_type gnb_id_{ unset_ID };
    Id_type ausf_id_{ unset_ID };

    Amf_fsm_map amf_fsms_;
};

class AMF_fsm
{
  public:
    using AMF_fn = Msg_response (AMF_fsm::*)(Byte_buffer const &str);
    // typedef Msg_response (AMF_fsm::*AMF_fn)(Byte_buffer const &str);
    AMF_fsm() = delete;
    AMF_fsm(AMF_fsm const &) = delete;
    AMF_fsm(
      Id_type amf_id, Id_type ausf_id, Id_type gnb_id, Msg_queue_ptr rq_ptr);
    Msg_response update_state(Byte_buffer const &bb);

  private:
    Id_type amf_id_{ unset_ID };
    Id_type gnb_id_{ unset_ID };
    Id_type ausf_id_{ unset_ID };
    Msg_queue_ptr router_queue_ptr_;
    Byte_buffer const abba_{ 0x00, 0x00 };// Send to UE
    Amf_ue_data ue_data_;
    Byte_buffer suci_;

    Byte_buffer rand_;
    Byte_buffer hxres_star_;

    // Used in N2 handovers
    Id_type srce_in_handover_{ unset_ID };
    Id_type target_in_handover_{ unset_ID };
    Id_type me_in_handover_{ unset_ID };
    Nasc_kamf_change_flag amf_change_flag_{ flag_unset };
    Byte_buffer k_amf_prime_{};
    Byte_buffer new_k_gnb_{};
    Next_hop_data new_nhd_{};
#ifdef MSG_TIMING
    std::string ho_type_{};
#endif
    AMF_fn state_ptr_;

    Msg_response idle(Byte_buffer const &bb);
    Msg_response awaiting_request(Byte_buffer const &bb);
    Msg_response awaiting_AV(Byte_buffer const &bb);
    Msg_response awaiting_me_response(Byte_buffer const &bb);
    Msg_response awaiting_confirmation(Byte_buffer const &bb);
    Msg_response active(Byte_buffer const &bb);
};
