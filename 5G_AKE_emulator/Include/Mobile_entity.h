/*******************************************************************************
 * File:        Mobile_entity.h
 * Description: Definition of the mobile entity class
 *
 * Created:     Thursday 21 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <string>
#include <array>

#include "Ecies_utils.h"
#include "Ecies.h"
#include "Ecies_aes.h"

#include "Byte_buffer.h"
#include "MT_queue.h"
#include "Msg_handler.h"
#include "Entity.h"
#include "Entity_shared_data.h"
#include "Authentication_utils.h"
#include "Model_param.h"

using ME_response = Byte_buffer;

class Mobile_entity : public Entity
{
  public:
    using me_fn = Msg_response (Mobile_entity::*)(Byte_buffer const &bb);
    // typedef Msg_response (Mobile_entity::*meFn)(Byte_buffer const &str);
    Mobile_entity(Id_type id, Usim const &us) : Entity(id), usim_(us)
    {
        initialise();
    }
    void insert_usim(Usim const &us) { usim_ = us; }
    void assign_amf_id(Byte sid) { amf_id_ = sid; }
    Byte_buffer get_suci() const { return ecies_data_[0]; }
    //    void assign_gnb_id(Byte sid) { gnb_id_ = sid; }

    Msg_response process_message(Byte_buffer const &bb) override;
    void report(std::ostream &os) const override;
    ~Mobile_entity() override;

  private:
    Usim usim_;
    Usim::Ecies_data ecies_data_;
    Byte_buffer crnti_;
    Byte_buffer sn_name_;
    Id_type gnb_id_{ unset_ID };
    Id_type amf_id_{ unset_ID };
    Byte_buffer k_gnb_;// The current K_gNB value
    Amf_ue_common_data
      amf_data_;// Data shared with AMF, K_gNB here does not change
    Next_hop_data current_next_hop_data_;

    // Temporary values for handover
    bool in_handover_{ false };
    Id_type gnb_id_star_{ unset_ID };
    Byte_buffer crnti_star_{ 0 };
    Byte_buffer k_gnb_star_;

    Byte_buffer k_amf_prime_{};
    Byte_buffer new_k_gnb_{};
    Next_hop_data new_nhd_{};
    Byte_buffer k_ng_ran_{};

    me_fn state_ptr_;

    void initialise();
    Msg_response idle(Byte_buffer const &bb);
    Msg_response waiting_crnti(Byte_buffer const &bb);
    Msg_response waiting_sn_id(Byte_buffer const &bb);
    Msg_response waiting_auth_response(Byte_buffer const &bb);
    Msg_response waiting_ack(Byte_buffer const &bb);
    Msg_response active(Byte_buffer const &bb);
};
