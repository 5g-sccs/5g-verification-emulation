/*******************************************************************************
 * File:        Model_param.h
 * Description: Definition of the model parameters
 *
 * Created:     Saturday 24 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <array>
#include <cstdint>
#include "Byte_buffer.h"

//#define DEBUG // Print extra information for debugging
//#define PRINT// Print the protocol messages
#define MSG_LOG// Print the actual messages sent to-and-fro
//#define MSG_COUNT// Count the data when doing handovers

//#define MSG_TIMING// Do the handover timing
// Turn off all of the other options to do the timing
#ifdef MSG_TIMING
#undef DEBUG
#undef PRINT
#undef MSG_COUNT
#undef MSG_LOG
#endif


using Id_type = Byte;

constexpr Id_type unset_ID = 0;
constexpr Id_type UE_base_ID = 1;
constexpr Id_type gNB_base_ID = 11;
constexpr Id_type amf_base_ID = 21;
constexpr Id_type ausf_base_ID = 31;
constexpr Id_type arpf_base_ID = 41;
constexpr Id_type router_base_ID = 51;
constexpr Id_type command_base_ID = 61;

// Command IDs used in source field for special commands
constexpr Id_type terminate_ID =
  0xFF;// Message to all entities to stop (i.e. termiante their thread)
constexpr Id_type me_attach_ID =
  0xFE;// Message to a UE to initiate a connection
constexpr Id_type gnb_ho_xn_ID =
  0xFD;// Message to a gNB to initiaite a handover using Xn interface
constexpr Id_type gnb_ho_n2_ID =
  0xFB;// Message to a gNB to initiate a handover using N2 interface
constexpr Id_type use_nh_ID =
  0xFC;// Message to a gNB to use any NH value that it holds (ie. mark the next
       // hop data as used)

// Reason codes related to protocol diagrams
enum : Byte {
    rc_attach =
      0x01,// Attach - command to UE to send a registration request to gNB
    rc_me_reg,// UE registration request to gNB (via rrc)
    rc_crnti,// gNB sends C_RNTI and AMF id to UE as a response to rc_me_reg
    rc_me_areg,// UE registration request to AMF (via N1)
    rc_sn_id,// AMF sends SN id to the ME
    rc_me_auth_init,// UE initiates the authentication process sending its SUCI
                    // and ECIES data to AMF (via N1)
    rc_auth_req,// Authentication data request (used for AMF->AUSF and
                // AUSF->ARPF)
    rc_auth_resp,// Authentication response (used for ARPF->AUSF and AUSF->AMF)
    rc_me_auth,// Authentication request to ME
    rc_me_auth_resp,// Authentication response from ME
    rc_amf_auth_resp,// Acknowledgement of authentication success (from
                     // AMF->AUSF)
    rc_ausf_auth_ack,// Acknowledgement of authentication success (from
                     // AUSF->AMF)
    rc_auth_ack,// Acknowledgement of authentication success (from AMF->gNB and
                // AMF->UE)
    rc_xn_ho_req = 0x20,// Handover request from old gNB to new gNB
    rc_xn_ho_acc,// Handover accept from new gNB to old gNB, or from ME to new
                 // gNB
    rc_xn_ho_me,// Handover message from old gNB to UE
    rc_xn_ho_cfrm,// Handover accept from ME to new gNB
    rc_xn_ho_comp,// Handover completion, message from old gNB to new gNB
    rc_xn_pdu_id,// Message from new gNB to AMF giving a new PDU Session-ID used
                 // here to prompt AMF for NH, NCC pair
    rc_xn_pdu_cfrm,// Message from AMF to new gNB giving an NH, NCC pair
    rc_n2_ho = 0x40,// Handover message from gNB to AMF (for N2 handover)
    rc_n2_ho_req,// Handover request from AMF to new gNB
    rc_n2_ho_ack,// Acknowledgement to AMF of handover request
    rc_n2_ho_cmd,// Handover message from AMF to source gNB and then to the ME
                 // (for N2 handover)
    rc_n2_ho_cfrm,// Handover message from the ME to the target gNB and then to
                  // the AMF (for N2 handover)
    rc_n2_ho_comp,// Handover message target gNB to the AMF and then to the
                  // source gNB (for N2 handover)
    rc_n2_ho_rel,// Handover message from AMF to source gNB releasing the UE
                 // context (for N2 handover)
    rc_n2_ho_done,// Handover message from source gNB to AMF confirming the UE
                  // context is released (for N2 handover)
};

// Message fields
constexpr Byte f_srce = 0;
constexpr Byte f_dest = 1;
constexpr Byte f_ref = 2;
constexpr Byte f_rc = 3;
constexpr Byte f_payl = 4;

// Reference points used to help the routing
enum Ref_point : Byte {
    cmd,// Not a reference point, used for command messages
    rrc,// not a reference point, but used here for the UE - gNB link
    n1,// UE - AMF/SEAF
    n2,// gNB -AMF
    xn,// gNB - gNB
    // n3,//gNB - UPF, not used here at the moment
    // n6,//UPF - DN, not used here at the moment
    // n8,// AMF/SEAF - ARPF/UDM not used here at the moment
    n12,// AMF/SEAF - AUSF
    n13// AUSF - ARPF/UDM
};

// Size in bytes of the different fields
constexpr Byte network_size = 3;
constexpr Byte msin_size = 5;
constexpr Byte supi_size = network_size + msin_size;
constexpr Byte suci_size = supi_size;
constexpr Byte rand_size = 16;
constexpr Byte amf_size =
  2;// Authentication Management Field, 33.102, 3GPP 33.102, Annex H
constexpr Byte sqn_size = 6;
constexpr Byte key_size = 16;
constexpr Byte mac_size = 8;
constexpr Byte xres_size = 16;
constexpr Byte xres_star_size = 16;
constexpr Byte ck_size = 16;
constexpr Byte ik_size = 16;
constexpr Byte ak_size = 6;
constexpr Byte autn_size = sqn_size + amf_size + mac_size;
constexpr Byte av_size = rand_size + xres_size + ck_size + ik_size + autn_size;
constexpr Byte iv_size = 16;
constexpr Byte mac_key_size = 32;
constexpr Byte mac_tag_size = 8;
constexpr Byte abba_size = 2;

constexpr Byte hdr_size =
  f_payl;// The size of the message header (source+dest+ref+rc)

// Parameters for USIM and ARPF

// Operator code, OP
const Byte_buffer opd{ "cdc202d5123e20f62b6d676ac72cb318" };
// Byte_buffer ec_key; // ECC public key

const std::string op_key_basename("prime256v1");

struct USIM_data
{
    Byte_buffer supi;
    Byte_buffer key;
    Byte_buffer sqn;
    Byte_buffer amf;// Authentication Management Field, 3GPP 33.102, Annex H
};

// <MCC>=234  <MNC>=10 PLMN=32f401 (see 36.413, 9.2.3.11 and 23.003, sec. 2.2)
// See:
// https://en.wikipedia.org/wiki/List_of_mobile_network_operators_of_Europe#United_Kingdom
const Byte_buffer plmn1{ Hex_string{ "32f401" } };// O2
const Byte_buffer plmn2{ Hex_string{ "32f433" } };// EE/BT group
const Byte_buffer plmn3{ Hex_string{ "32f451" } };// Vodafone

// SUPI - use IMSI based SUPI starts with PLMN (see 36.413, 9.2.3.11 and 23.003
// sec. 2.2)
const USIM_data usimd1{
    plmn1 + Byte_buffer{ Hex_string{ "21436587f9" } },
    Byte_buffer{ Hex_string{ "465b5ce8b199b49faa5f0a2ee238a6bc" } },
    Byte_buffer{ Hex_string{ "ff9bb4d0b607" } },
    Byte_buffer{ Hex_string{ "0005" } }// Arbitrary proprietary bits
};
const USIM_data usimd2{
    plmn1 + Byte_buffer{ Hex_string{ "89674523f1" } },
    Byte_buffer{ Hex_string{ "9f7c8d021accf4db213ccff0c7f71a6a" } },
    Byte_buffer{ Hex_string{ "9d0277595ffc" } },
    Byte_buffer{ Hex_string{ "000c" } }// Arbitrary proprietary bits
};

const Byte_buffer d_five_g{ '5', 'G', ':' };
const Byte_buffer d_sn_id{ 0x32, 0x64, 0x7f };
const Byte_buffer d_sn_name = d_five_g + d_sn_id;
const auto sn_name_size = d_sn_name.size();

// C-RNTI - see 36.231, Table 7.7-1
constexpr Byte crnti_size = 2;

// Parameters for AMF/SEAF
// GUAMI - Globally Unique AMF Identifier: <MCC><MNC><AMFI>. See 23.003
// Sec. 2.10 AMFI - AMF Identifier: <AMF Region ID><AMF Set ID><AMF pointer> AMF
// Pointer identifies one or more AMFs withing the AMF Set


// Items for printing
const std::string la(" <----------------- ");// left arrow
const std::string ra(" -----------------> ");// right arrow
const std::string da(" <----------------> ");// double headed
const std::string m_to_g("UE" + ra + "gNB");
const std::string m_to_s("UE" + ra + "AMF");
const std::string s_to_m("UE" + la + "AMF");
const std::string g_space("           ");
const std::string g_to_m("UE" + la + "gNB");
const std::string g_to_g(g_space + "gNB" + da + "gNB");
const std::string a_to_g(g_space + "gNB" + la + "AMF");
const std::string g_to_a(g_space + "gNB" + ra + "AMF");
const std::string as_space("                      ");
const std::string s_to_a(as_space + "AMF" + ra + "AUSF");
const std::string a_to_s(as_space + "AMF" + la + "AUSF");
const std::string aa_space(as_space + as_space);
const std::string ra_to_a(aa_space + "AUSF" + ra + "ARPF");
const std::string la_to_a(aa_space + "AUSF" + la + "ARPF");
