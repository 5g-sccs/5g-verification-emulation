/*******************************************************************************
 * File:        MT_queue.h
 * Description: Mulit-threaded queue
 *
 * Created:     Tuesday 20 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <stdexcept>
#include <condition_variable>
#include <mutex>
#include <queue>

// Thread safe print routine to use with the queues
extern std::mutex print_mutex;
void locked_print(std::ostream &os, std::string const &str);

template<class T> class MT_queue
{
  public:
    using Element_type = T;
    using Queue_type = MT_queue<T>;
    using Queue_ptr = MT_queue<T> *;
    MT_queue() = default;
    MT_queue(const MT_queue &) = delete;
    MT_queue(MT_queue &&) = delete;
    MT_queue &operator=(const MT_queue &) = delete;
    void push_message(Element_type const &str);
    Element_type pop_message();
    ~MT_queue() = default;

  private:
    std::queue<Element_type> queue_;
    std::condition_variable cond_;
    std::mutex mutex_;
};

template<class T> T MT_queue<T>::pop_message()
{
    std::unique_lock<std::mutex> ul(mutex_);
    if (queue_.empty()) {
        cond_.wait(ul, [this] { return !queue_.empty(); });
    }
    auto el = queue_.front();
    queue_.pop();
    ul.unlock();

    return el;
}

template<class T> void MT_queue<T>::push_message(Element_type const &str)
{
    std::unique_lock<std::mutex> lck(mutex_);
    queue_.push(str);
    lck.unlock();
    cond_.notify_one();
}

// Assign a fixed number of queues, for now
template<class T, size_t N> class MT_queue_mgr
{
  public:
    using Queue_type = MT_queue<T>;
    using Queue_ptr = MT_queue<T> *;
    MT_queue_mgr() : next_queue_(0) {}
    Queue_ptr get_queue();
    ~MT_queue_mgr() {}

  private:
    Queue_type queues_[N];
    int next_queue_;
};

template<class T, size_t N> MT_queue<T> *MT_queue_mgr<T, N>::get_queue()
{
    if (next_queue_ >= N) {
        throw(std::length_error("No more queues available"));
    }
    return &queues_[next_queue_++];
}
