/*******************************************************************************
 * File:        Msg_utils.h
 * Description: Message utilities
 *
 * Created:     Monday 30 August 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <iostream>
#include <map>
#include "Byte_buffer.h"
#include "Clock_utils.h"

using Msg_timer = F_timer_mu;
using Msg_timer_data = Timing_data<Msg_timer::Rep>;
using Msg_timings = Timings<Msg_timer::Rep>;

// Quick hack to get some data for the paper - make these global
struct Ho_msg_data_count
{
    bool in_handover{ false };
    std::string type{};
    uint32_t byte_count{ 0 };
};
using Ho_count_data = std::vector<Ho_msg_data_count>;
extern Ho_msg_data_count ho_msg_count;
extern Ho_count_data ho_msg_counts;
void reset_ho_msg_count();

extern Msg_timer ho_timer;
extern Msg_timings ho_timings;

void debug_message(std::ostream &os, Byte_buffer const &msg);

void print_message(std::ostream &os, Byte_buffer const &msg);

std::string entity_str(Id_type id);

std::string reference_point(Byte rp);

const std::map<Byte, std::string> rc_map{
    { rc_attach,
      "" },// Attach - command to UE to send a registration request to gNB
    { rc_me_reg, " gNB reg  " },// UE registration request to gNB (via rrc)
    { rc_crnti, "send crnti" },// gNB sends C_RNTI and AMF id to UE as a
                               // response to rc_me_reg
    { rc_me_areg, " AMF reg  " },// UE registration request to AMF (via N1)
    { rc_sn_id, "send SN id" },// AMF sends SN id to the ME
    { rc_me_auth_init,
      "auth init " },// UE initiates the authentication process sending its SUCI
                     // and ECIES data to AMF (via N1)
    { rc_auth_req, "auth req  " },// Authentication data request (used for
                                  // AMF->AUSF and AUSF->ARPF)
    { rc_auth_resp, "auth resp " },// Authentication response (used for
                                   // ARPF->AUSF and AUSF->AMF)
    { rc_me_auth, " auth UE  " },// Authentication request to ME
    { rc_me_auth_resp, " ME resp  " },// Authentication response from ME
    { rc_amf_auth_resp,
      " AMF resp " },// Acstd::string rc_str(Byte rc);er accept from new gNB to
                     // old gNB, or from ME to new gNB
    { rc_ausf_auth_ack, " auth ack " },// Acknowledgement of authentication
                                       // success (from AUSF->AMF)
    { rc_auth_ack, " auth ack " },// Acknowledgement of authentication success
                                  // (from AMF->gNB and AMF->UE)
    { rc_xn_ho_req, " Xn ho req " },// Handover request from old gNB to new gNB
    { rc_xn_ho_acc, " Xn ho ack " },// Handover accept from new gNB to old gNB,
                                    // or from ME to new gNB
    { rc_xn_ho_me, " Xn ho UE  " },// Handover message from old gNB to UE
    { rc_xn_ho_cfrm, " UE ho acc " },// Handover accept from ME to new gNB
    { rc_xn_ho_comp,
      "Xn ho comp " },// Handover completion, message from old gNB to new gNB
    { rc_xn_pdu_id,
      " Xn PDU id " },// Message from new gNB to AMF giving a new PDU Session-ID
                      // used here to prompt AMF for NH, NCC pair
    { rc_xn_pdu_cfrm,
      "  AMF ack  " },// Message from AMF to new gNB giving an NH, NCC pair
    { rc_n2_ho,
      " N2 ho req " },// Handover message from gNB to AMF (for N2 handover)
    { rc_n2_ho_req, "AMF ho req " },// Handover request from AMF to new gNB
    { rc_n2_ho_ack,
      " N2 ho ack " },// Acknowledgement to AMF of handover request
    { rc_n2_ho_cmd, " N2 ho cmd " },// Handover message from AMF to source gNB
                                    // and then to the ME (for N2 handover)
    { rc_n2_ho_cfrm,
      "UE ho cfrm " },// Handover message from the ME to the target gNB and then
                      // to the AMF (for N2 handover)
    { rc_n2_ho_comp,
      "N2 ho comp " },// Handover message target gNB to the AMF and then to the
                      // source gNB (for N2 handover)
    { rc_n2_ho_rel,
      "gNB release" },// Handover message from AMF to source gNB releasing the
                      // UE context (for N2 handover)
    { rc_n2_ho_done,
      "gNB confirm" }// Handover message from source gNB to AMF confirming the
                     // UE context is released (for N2 handover)
};

std::string rc_str(Byte rc);
