/*******************************************************************************
*
 * File:        milenage_f1to5.cpp
* Description: Code for the milenage set of
 * functions
*
* Created:     Wednesday 28 February
 * 2018
*
*              Adapted from 3GPPO
 * 35206-e00
*
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <cstdint>

typedef uint8_t u8;// Left in from 3GPP version

/*--------- Operator Variant Algorithm Configuration Field --------*/
extern u8 OP[16];

/*--------------------------- prototypes --------------------------*/

void f1(u8 k[16], u8 rand[16], u8 sqn[6], u8 amf[2], u8 mac_a[8]);
void f2345(u8 k[16], u8 rand[16], u8 res[8], u8 ck[16], u8 ik[16], u8 ak[6]);
void f1star(u8 k[16], u8 rand[16], u8 sqn[6], u8 amf[2], u8 mac_s[8]);
void f5star(u8 k[16], u8 rand[16], u8 ak[6]);
void ComputeOPc(u8 op_c[16]);
void RijndaelKeySchedule(u8 key[16]);
void RijndaelEncrypt(u8 input[16], u8 output[16]);


/*-------------------- Rijndael round subkeys ---------------------*/
extern u8 roundKeys[11][4][4];

/*--------------------- Rijndael S box table ----------------------*/
extern u8 S[256];

/*------- This array does the multiplication by x in GF(2^8) ------*/
extern u8 Xtime[256];

/* Round key addition function */
void KeyAdd(u8 state[4][4], u8 roundKeys[11][4][4], int round);

/* Byte substitution transformation */
int ByteSub(u8 state[4][4]);

/* Row shift transformation */
void ShiftRow(u8 state[4][4]);

/* MixColumn transformation*/
void MixColumn(u8 state[4][4]);
