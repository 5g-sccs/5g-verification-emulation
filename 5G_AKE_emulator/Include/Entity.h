/*******************************************************************************
 * File:        Entity.h
 * Description: The base class entity
 *
 * Created:     Monday 21 June 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <iostream>
#include <string>
#include "Byte_buffer.h"
#include "MT_queue.h"
#include "Msg_handler.h"
#include "Model_param.h"

constexpr int max_number_of_queues = 10;
using Entity_queues = MT_queue_mgr<Byte_buffer, max_number_of_queues>;

class Entity
{
  public:
    Entity() = delete;
    Entity(Entity const &) = default;
    Entity(Id_type id) : id_(id) {}
    Entity &operator=(Entity const &) = default;
    Entity &operator=(Entity &&) = default;
    void assign_input_queue(Msg_queue_ptr q_ptr) { input_queue_ptr_ = q_ptr; }
    void assign_router_queue(Msg_queue_ptr q_ptr) { router_queue_ptr_ = q_ptr; }
    Id_type get_id() const { return id_; }
    Msg_queue_ptr get_input_queue_ptr() { return input_queue_ptr_; }
    Msg_queue_ptr get_router_queue_ptr() { return router_queue_ptr_; }
    virtual Msg_response process_message(Byte_buffer const &msg) = 0;
    virtual void report(std::ostream &os) const {};
    virtual ~Entity(){};

  protected:
    Id_type id_{ unset_ID };
    bool keep_running_{ false };
    Msg_queue_ptr input_queue_ptr_{ nullptr };
    /*
     * router_queue_ptr only used for sending multiple responses from a single
     * message - normally just use the return from the state handling the
     * message
     */
    Msg_queue_ptr router_queue_ptr_{ nullptr };

  private:
};

using Entity_msg_handler = Msg_handler<Entity, Id_type>;
