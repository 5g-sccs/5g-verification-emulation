/*******************************************************************************
 * File:        Msg_handler.h
 * Description: A template class for thread message handling
 *
 * Created:     Wednesday 7 March 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include "Byte_buffer.h"
#include "Io_utils.h"
#include "MT_queue.h"
#include "Model_param.h"

using Msg_queue = MT_queue<Byte_buffer>;
using Msg_queue_ptr = Msg_queue *;
using Msg_response = std::optional<Byte_buffer>;

template<class E, class I> class Msg_handler
{
  public:
    using Entity_type = E;
    using Entity_ptr = E *;
    using Id_type = I;
    // using Queue_map = std::map<Id_type, Msg_queue_ptr>;
    Msg_handler() = delete;
    Msg_handler(Id_type e_id, Entity_ptr e_ptr)
      : entity_id_(e_id), e_ptr_(e_ptr)
    {}
    void assign_input_queue(Msg_queue_ptr iq) { input_queue_ptr_ = iq; }
    void assign_router_queue(Msg_queue_ptr rq) { router_queue_ptr_ = rq; }

    Id_type get_id() const { return entity_id_; }
    Msg_queue_ptr get_input_queue_ptr() const { return input_queue_ptr_; }
    void handle_message(Byte_buffer const &bb);
    void operator()();

  private:
    Id_type entity_id_{ 0 };
    Entity_ptr e_ptr_{ nullptr };
    bool keep_running{ false };
    Msg_queue_ptr input_queue_ptr_{ nullptr };
    Msg_queue_ptr router_queue_ptr_{ nullptr };
};

template<class E, class I> void Msg_handler<E, I>::operator()()
{
    if (input_queue_ptr_ == nullptr || router_queue_ptr_ == nullptr) {
        locked_print(std::cerr, "Msg_handler: not correctly initialised\n");
        return;
    }
    keep_running = true;
    while (keep_running) {
        auto ocs = input_queue_ptr_->pop_message();
        handle_message(ocs);
    }
}

template<class E, class I>
void Msg_handler<E, I>::handle_message(Byte_buffer const &bb)
{
    Id_type source = bb[f_srce];
    Id_type dest = bb[f_dest];
    std::ostringstream os;
#ifdef DEBUG
    os << "Handling message: " << bb << '\n';
    locked_print(std::cout, os.str());
    os.str("");
#endif
    if (dest != entity_id_) {
        os << "Bad id in message: " + bb.to_hex_string() + '\n';
        locked_print(std::cout, os.str());
        os.str("");
        return;
    }
    if (source == terminate_ID) {
#ifdef DEBUG
        os << "Terminate called for id: " << 0 + dest << '\n';
        locked_print(std::cout, os.str());
        os.str("");
#endif
        keep_running = false;
    } else {
        Msg_response response = e_ptr_->process_message(bb);
        if (response == std::nullopt) { return; }

        router_queue_ptr_->push_message(response.value());
    }
}
