/*******************************************************************************
 * File:        Msg_router.h
 * Description: A template class for message routing
 *
 * Created:     Monday 21 June 2021
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <map>
#include <thread>
#include "Byte_buffer.h"
#include "MT_queue.h"
#include "Msg_handler.h"
#include "Msg_utils.h"
#include "Model_param.h"

template<class I> class Msg_router
{
  public:
    using Id_type = I;
    using Queue_map = std::map<Id_type, Msg_queue_ptr>;
    Msg_router() = delete;
    Msg_router(Id_type id) : id_(id) {}
    void assign_input_queue(Msg_queue_ptr iq) { input_queue_ptr_ = iq; }
    void register_entity_queue(Id_type id, Msg_queue_ptr qptr);
    Id_type get_id() const { return id_; }
    Msg_queue_ptr get_input_queue_ptr() const { return input_queue_ptr_; }
    void handle_message(Byte_buffer const &bb);
    void operator()();
    ~Msg_router();

  private:
    Id_type id_{ 0 };
    bool keep_running{ false };
    Msg_queue_ptr input_queue_ptr_{ nullptr };
    Queue_map entity_queue_ptrs_;
};

template<class I> Msg_router<I>::~Msg_router()
{
#ifdef DEBUG
    std::ostringstream os;
    os << "Msg_router " << 0 + id_ << " destructor\n";
    locked_print(std::cout, os.str());
#endif
}

template<class I>
void Msg_router<I>::register_entity_queue(Id_type id, Msg_queue_ptr q_ptr)
{
    auto result = entity_queue_ptrs_.emplace(id, q_ptr);
    std::ostringstream os;
#ifdef DEBUG
    os << "Inserting " << 0 + id << " into the queue pointers map\n";
    locked_print(std::cout, os.str());
    os.str("");
#endif
    if (!result.second) {
        os << "Pointer for ID " << 0 + id
           << " already inserted, list unchanged\n";
        locked_print(std::cout, os.str());
    }
    os.str("");
#ifdef DEBUG
    os << "Map has " << entity_queue_ptrs_.size() << " elements\n";
    locked_print(std::cout, os.str());
#endif
}

template<class I> void Msg_router<I>::operator()()
{
    if (entity_queue_ptrs_.empty()) {
        locked_print(std::cout, "Msg_handler: not correctly initialised\n");
        return;
    }
    keep_running = true;
    while (keep_running) {
        auto ocs = input_queue_ptr_->pop_message();
        handle_message(ocs);
    }
}

template<class I> void Msg_router<I>::handle_message(Byte_buffer const &bb)
{
    Id_type source = bb[f_srce];
    Id_type dest = bb[f_dest];

    if (source == terminate_ID) {
        keep_running = false;
        return;
    }
#ifdef MSG_LOG
    print_message(std::cout, bb);
#endif
#ifdef MSG_COUNT
    if (ho_msg_count.in_handover) { ho_msg_count.byte_count += bb.size(); }
#endif
    auto const pos = entity_queue_ptrs_.find(dest);
    if (pos != entity_queue_ptrs_.end()) {
        pos->second->push_message(bb);
    } else {
        std::ostringstream os;
        os << "No queue pointer for destination " << dest << '\n';
        locked_print(std::cout, os.str());
        os.str("");
        os << "Router map has " << entity_queue_ptrs_.size() << " elements\n";
        locked_print(std::cout, os.str());
    }
}

using Mesg_router = Msg_router<Id_type>;
