/*******************************************************************************
 * File:        Arpf_entity.h
 * Description: Definition of the ARPF class
 *
 * Created:     Thursday 21 February 2018
 *
 *
*******************************************************************************/

/*******************************************************************************
*                                                                              *
* The 5G Key-Establishment Stack:                                              *
* ===============================                                              *
* In-Depth Formal Verification & Experimentation                               *
* ==============================================                               *
*                                                                              *
* This code has been anonymised and is provided solely to aid the review of    *
* the article titled above. It should not be redistributed in source or binary *
* form, with or without, modification. A BSD licensed version of the code will *
* be made availble in due course.                                              *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*                                                                              *
*******************************************************************************/


#pragma once

#include "Ecies_utils.h"
#include "Ecies.h"
#include "Ecies_aes.h"

#include "Byte_buffer.h"
#include "Hmac.h"
#include "MT_queue.h"
#include "Msg_handler.h"
#include "Model_param.h"
#include "Entity.h"

class ARPF_fsm;

class ARPF_entity : public Entity
{
  public:
    friend ARPF_fsm;
    using Subscribers = std::map<Byte_buffer, USIM_data>;
    using Arpf_fsm_map = std::map<Byte_buffer, ARPF_fsm *>;
    ARPF_entity(Id_type id, Byte_buffer opb, EC_KEY *eck)
      : Entity(id), op_(opb), ec_priv_key_(eck)
    {}
    Byte_buffer get_ecies_data(Byte_buffer const &bb) const;
    void assign_ausf_id(Id_type au_id) { ausf_id_ = au_id; }
    void add_user(USIM_data const &ud);
    Byte_buffer generate_HE_AV(
      USIM_data const &usd, Byte_buffer const &sn_name) const;

    Msg_response process_message(Byte_buffer const &bb) override;
    void report(std::ostream &os) const override;
    ~ARPF_entity() override;

  private:
    Id_type ausf_id_{ unset_ID };
    Byte_buffer op_;// Operator code
    EC_KEY *ec_priv_key_{ nullptr };// Private ECC key
    Subscribers users_;
    Arpf_fsm_map arpf_fsms_;
};


class ARPF_fsm
{
  public:
    friend ARPF_entity;
    using arpf_fn = Msg_response (ARPF_fsm::*)(Byte_buffer const &bb);
    // typedef Msg_response (ARPF_fsm::*arpf_fn)(Byte_buffer const &bb);
    ARPF_fsm() = delete;
    ARPF_fsm(ARPF_fsm const &) = delete;
    ARPF_fsm(Id_type id,
      Id_type au_id,
      Byte_buffer const &suci,
      USIM_data const &ud,
      ARPF_entity *arpf_ptr)
      : id_(id), ausf_id_(au_id), suci_(suci), usd_(ud), arpf_ptr_(arpf_ptr)
    {
        state_ptr_ = &ARPF_fsm::new_request;
    };
    Msg_response update_state(Byte_buffer const &bb);

  protected:
  private:
    Id_type id_{ unset_ID };
    Id_type ausf_id_{ unset_ID };
    Byte_buffer suci_;
    Byte_buffer sn_name_;
    USIM_data usd_;
    ARPF_entity *arpf_ptr_;

    arpf_fn state_ptr_;

    Msg_response new_request(Byte_buffer const &bb);
    Msg_response active(Byte_buffer const &bb);
};
